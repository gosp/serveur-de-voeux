-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 25 Juin 2014 à 22:48
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `voeux`
--

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `titre` varchar(1000) NOT NULL,
  `reponse` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `questions`
--

INSERT INTO `questions` (`id`, `type`, `titre`, `reponse`) VALUES
(1, 'prive', 'Pourquoi je ne peux pas m''inscrire à un module ?', 'Pas de panique, ce problème est tout à fait normal. Plusieurs causes sont susceptibles de vous empêcher de vous inscrire. Peut-être avez vous oublié de vous connecter, c''est pourquoi vous n''accédez pas à la page vous permettant de vous inscrire. Ensuite, êtes-vous sûr que votre statut est actif ? Si ce n''est pas le cas, consultez la question "Mon compte n''est pas actif, que faire ?".'),
(2, 'prive', 'A quoi sert la page de récapitulatif ?', 'Cette page regroupe toutes les informations d''un enseignant. Elles sont consultables via une recherche sur l''enseignant dont vous souhaitez visiter la page. Elle contient entre autres des informations sur le nom, le prénom, le statut, les modules auxquels est inscrit l''enseignant ainsi que des statistiques. Lorsque vous êtes connecté, vous avez accès à votre récapitulatif en cliquant sur le lien "Récapitulatif".'),
(3, 'prive', 'Quelles fonctionnalités supplémentaires ont les administrateurs ?', 'Les administrateurs sont présents pour gérer les différents comptes enseignants. Ils peuvent bien entendu transférer leurs droits, créer des comptes pour les nouveaux enseignants, ainsi que les supprimer. De plus,ils ont aussi la possibilité de créer, supprimer ou modifier les différents modules, ou encore de modifier les affectations d''un enseignant à un module. Enfin, ce sont également les administrateurs qui modifient les décharges des enseignants. '),
(4, 'prive', 'Mon compte n''est pas actif, que faire ?', 'Si votre compte est inactif, il vous suffit de faire une demande auprès d''un administrateur directement afin que l''état actif de votre compte soit mis à jour. Ces derniers ayant des droits supplémentaires, cela leur permettra de modifier l''état de votre compte.'),
(5, 'public', 'Comment me connecter ?', 'Afin de vous connecter via votre compte enseignant, il suffit de cliquer sur le lien en haut à droite de la page. Après avoir cliqué sur "se connecter", saisissez vos identifiants puis cliquez sur "Valider".'),
(6, 'public', 'Comment obtenir un compte enseignant?', 'Si l''administrateur ne vous a pas fourni vos identifiants, n''hésitez pas à lui redemander de vous créer un compte. Cette opération sera très rapide. De plus, cela vous permet de vous sociabiliser avec vos nouveaux collègues afin de trouver qui est l''administrateur.'),
(7, 'public', 'A quoi sert ce site ?', 'Comme dit sur la page d''accueil, il s''agit d''un serveur de voeux. Un serveur de voeux est un outil facilitant la répartition des modules aux enseignants de l''université. En effet, plutôt que de devoir se concerter régulièrement pour les partager, en prenant le risque de commettre des erreurs, cet outil permet d''assurer une sécurisation de la répartition.'),
(8, 'public', 'A qui est destiné l''usage de ce site ?', 'Ce serveur de voeux est destiné aux enseignants de l''ENSSAT. Chaque école peut avoir son propre serveur de voeux. Cet outil n''est pas accessible par des personnes externes à l''université, ni par les élèves eux-même.'),
(9, 'prive', 'J''ai oublié mes identifiants, que faire ?', 'Pas d''inquiétude, cela ne pose aucun problème particulier. S''il s''agit d''un oubli de votre nom de compte, le format est le même pour tous les utilisateurs. Votre login est composé de la première lettre de votre prénom, suivie de votre nom. Cependant, si vous avez oublié votre mot de passe, vous pouvez demander à un administrateur de vous en fournir un nouveau, ou de vous le rappeler (les administrateurs ayant accès à la base de données).'),
(10, 'prive', 'J''ai décelé un bug, comment le signaler ?', 'Cet outil ayant été mis en place récemment, il n''existe pas de fonctionnalité permettant le signalement d''un bug en ligne. Le mieux que vous puissiez donc faire est de contacter directement un administrateur (pause café, mail..)'),
(11, 'prive', 'J''ai une nouvelle idée de module, comment le mettre en place ?', 'Cette fonctionnalité est réservée aux administrateurs. Si vous le souhaitez, vous pouvez demander le rang d''administrateur afin de pouvoir créer le module en question. Néanmoins, si devenir administrateur ne vous intéresse pas, vous pouvez contacter un enseignant administrateur afin qu''il mette en place ce module pour vous.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
