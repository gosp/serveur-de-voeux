<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

	/**
	 * Données à envoyer aux view
	 * @var array
	 */
	public $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->set_base_data();
	}


	/**
	 * Set le tableau de données de base pour les views
	 */
	private function set_base_data()
	{
		if($this->session->userdata('connected'))
		{
			$this->data['connected'] = true;
			$this->data['user']['admin'] = $this->user_rights('administrateur');
		}
		else
		{
			$this->data['connected'] = false;
		}

		$this->data['current_page'] = $this->uri->segment(1);

		// On stock la page précédente
		if (isset($_SERVER['HTTP_REFERER']))
		{
			$this->session->set_userdata('previous_page', $_SERVER['HTTP_REFERER']);
		}
		else
		{
			$this->session->set_userdata('previous_page', base_url());
		}
	}

	/**
	 * Ajoute des données à $this->data
	 * @param mixed $data
	 */
	protected function add_data(array $data)
	{
		$this->data = array_merge($this->data, $data);
	}

	/**
	 * Vide le tableau des données des view
	 */
	protected function clear_data(array $data)
	{
		$this->data = array();
		$this->set_base_data();
	}	

	/**
	 * Insère un message d'erreur à afficher
	 * @param String $message Le message d'erreur
	 * @param Array $data informations supplémentaires
	 */
	protected function error_message($message, $data = array())
	{
		$this->data['error_message'] = $message;
		$this->data['error_details'] = $data;
	}

	/**
	 * Affiche la page d'erreur
	 * @param String $error_type le type de page à afficher
	 */
	protected function error_show($error_type)
	{
		$this->data['_redirect_page'] = $this->session->userdata('previous_page');
		$this->load->view($error_type, $this->data);
	}

	/**
	 * Test si l'utilisateur est connecté
	 * @return Boolean
	 */
	protected function user_connected()
	{
		if($this->session->userdata('connected')) return TRUE;
		else return FALSE;
	}

	/**
	 * Test si l'utilisateur a les droits demandés
	 * @param  String rights_type le type de droits à tester
	 * @return Boolean
	 */
	protected function user_rights($rights_type)
	{
		if($this->session->userdata($rights_type) == 1) return TRUE;
		else return FALSE;
	}

	/**
	 * Set un message d'erreur ou d'information ou succèss pour la page courante
	 * Pour des messages différés (après redirect) voir flashdata
	 * @param  String $message_type
	 * @param  String $message
	 * @return Boolean
	 */
	protected function set_alert($message_type, $message)
	{
		$this->data["_".$message_type] = $message;
	}

	/**
	 * Vérifie l'existence du paramètre soit dans POST, soit dans GET
	 * @param String $param de type POST ou GET
	 * @return Boolean
	 */
	protected function param_exists($param, $method = "both"){
		if($method == 'post')
		{
			if(isset($_POST[$param]))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if($method == 'get')
		{
			 if(isset($_GET[$param]))
			 {
			 	return true;
			 }
			 else
			 {
			 	return false;
			 }
		}
		else
		{
			 if(isset($_POST[$param]) || isset($_GET[$param])){
			 	return true;
			 }
			 else
			 {
			 	return false;
			 }
		}
	}

}

/* End of file index.php */
/* Location: ./application/controllers/index.php */