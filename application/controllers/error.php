<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Error extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	// Affiche la page home
	public function index()
	{		
		$this->error_show('error_404');
	}	

}
