<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Admin_affectation extends Base_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->model ( "module" );
		$this->load->model ( "contenu" );
		$this->load->model ( "user" );
		$this->load->model ( "admin" );
	}

	/**
	 * Vérifie les droits d'accès aux pages
	 * et charge les pages d'erreur en cas de besoin
	 * @return Boolean
	 */
	private function checkAccess(){
		if($this->user_connected())
		{
			if(!$this->user_rights('administrateur'))
			{
				$this->error_message("Vous devez être administrateur pour consulter cette page");
				$this->error_show('error_allowed');
				return false;
			}
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
			return false;
		}
		return true;
	}

	/**
	 * Ajoute une affectation
	 */
	public function addCourse(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('module', 'Module', 'required');
			$this->form_validation->set_rules('partie', 'Partie', 'required');
			$this->form_validation->set_rules('type', 'Type', 'required');
			$this->form_validation->set_rules('hed', 'Nombre heures', 'required');
			$this->form_validation->set_rules('enseignant', 'Enseignant', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$module = $this->input->post('module');
				$partie = $this->input->post('partie');
				$type = $this->input->post('type');
				$hed = $this->input->post('hed');
				$enseignant = $this->input->post('enseignant');

				$this->admin->addCourse($module,$partie,$type,$hed,$enseignant);
				$this->session->set_flashdata('success_message', 'Partie '. $partie .' ajoutée avec succès dans le module '. $module);
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs');
			}

			redirect('/admin_affectation');
		}
	}
	
	
	/**
	 * Edite une affectation
	 */
	public function editCourse(){
		if($this->checkAccess())
		{
			$module = $this->input->post('module');

			// Demande d'édition
			if( $this->input->post('edit1Course') == 1)
			{
				$this->data['edit1Course']['state'] = 1;
				$this->data['edit1Course']['data']['selected_module'] = $module;
				$this->data['edit1Course']['data']['content'] = $this->contenu->get_module_contenu($module);

				$this->set_alert('info_message', 'sélection du module '.$module.' réussie..');
				$this->index();
			}
			// Une fois le module et la partie choisie, on peut modifier
			else if( $this->input->post('edit2Course') == 1)
			{
				$partie = $this->input->post('partie');

				$this->data['edit2Course']['state'] = 1;
				$this->data['edit2Course']['data']['selected_module'] = $module;
				$this->data['edit2Course']['data']['selected_partie'] = $partie;
				$this->data['edit2Course']['data']['content'] = $this->contenu->get_module_part_content($module, $partie);

				$this->set_alert('info_message', 'édition de '.$partie.' en cours..');
				$this->index();				
			}
			else // modification d'une affectation
			{
				$this->form_validation->set_rules('module', 'Module', 'required');
				$this->form_validation->set_rules('partie', 'Partie', 'required');
				$this->form_validation->set_rules('type', 'Type', 'required');
				$this->form_validation->set_rules('hed', 'Hed', 'required');
				$this->form_validation->set_rules('enseignant', 'Enseignant', 'required');

				if ($this->form_validation->run() == TRUE)
				{
					$module = $this->input->post('module');
					$partie = $this->input->post('partie');
					$type = $this->input->post('type');
					$hed = $this->input->post('hed');
					$enseignant = $this->input->post('enseignant');
					$this->admin->editCourse($module,$partie,$type,$hed,$enseignant);	
					$this->session->set_flashdata('success_message', 'Enseignant modifié avec succès');	
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs');
				}
				redirect('/admin_affectation');
			}
		}
	}
	
	/**
	 * Supprime une affectation
	 */
	public function deleteCourse(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('module', 'Module', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$module = $this->input->post('module');

				if( $this->input->post('deleteCourse') == 1)
				{
					$this->data['deleteCourse']['state'] = 1;
					$this->data['deleteCourse']['data']['selected_module'] = $module;
					$this->data['deleteCourse']['data']['content'] = $this->contenu->get_module_contenu($module);

					$this->set_alert('info_message', 'sélection du module '.$module.' réussie..');
					$this->index();
				}
				else
				{
					$this->form_validation->set_rules('module', 'Module', 'required');
					
					if ($this->form_validation->run() == TRUE)
					{
						$partie = $this->input->post('partie');
						$this->admin->deleteCourse($module,$partie);
						$this->session->set_flashdata('success_message', 'Partie supprimée avec succès');	
					}
					else
					{
						$this->session->set_flashdata('error_message', 'Impossible vous n\'avez pas renseigné la partie à supprimer');
					}
		
					redirect('/admin_affectation');
				}
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Impossible vous n\'avez pas renseigné le module à supprimer');
				redirect('/admin_affectation');
			}
		}
	}
	
	/**
	 * Recherche des affectations
	 */
	public function searchCourse(){
		if($this->checkAccess())
		{			
			$module = $this->input->post('module');
			$partie = $this->input->post('partie');
			$type = $this->input->post('type');
			$enseignant = $this->input->post('enseignant');
		
			$this->data ['course_info'] = $this->admin->searchCourse ($module,$partie,$type,$enseignant);
			$this->data ['modules'] = $this->admin->get_all_modules ();
			$this->data ['enseignants'] = $this->admin->get_table_data ('enseignant');

			$this->load->view ( 'admin_affectation', $this->data );
		}
	}
	
	/**
	 * affiche la page administrateur de gestion des affectations
	 */
	public function index(){
		if($this->checkAccess())
		{	
			$this->data ['course_info'] = $this->admin->get_table_data ('contenu');
			$this->data ['enseignants'] = $this->admin->get_all_teacher_login();
			$this->data ['modules'] = $this->module->get_all_modules ();
			$this->data ['parties'] = $this->contenu->get_all_parts ();

			$this->load->view ( 'admin_affectation', $this->data );
		}
	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */