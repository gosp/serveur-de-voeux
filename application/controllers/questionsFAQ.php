<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class questionsFAQ extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("questions");
	}

	// Affiche la FAQ
	public function index()
	{	
		if($this->user_connected())
		{
			$this->data['questions'] = $this->questions->get_all_questions();
		}
		else
		{
			$this->data['questions'] = $this->questions->get_questions_not_connected();
		}
			$this->load->view('questions', $this->data);
	}
}
