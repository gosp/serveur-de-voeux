<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Home extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("module");
	}

	// Affiche la page home
	public function index()
	{	
		$this->load->view('home', $this->data);
	}	

}