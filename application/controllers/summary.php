<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Summary extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("module");
		$this->load->model("contenu");
		$this->load->model("user");
		$this->load->helper("image_helper");
	}

	/**
	 * Affiche le récapitulatif de l'utilisateur
	 * connecté
	 */
	public function index(){
		if($this->user_connected())
		{
			$this->add_data($this->getUserRecap($this->session->userdata('login')));
			$this->data['user_info'] = $this->session->all_userdata();

			// Génération des liens de désinscription
			foreach ($this->data['modules'] as &$module) 
			{
				foreach ($module['parts'] as &$part) 
				{
					// Lien pour se désinscrire
					$part['links']["unsubscribeLink"] = "/summary/unsubscribe/".
					$this->data['user_info']['login']."/".$module['infos']["ident"]."/".$part["partie"];
				}
			}
			$this->load->view('summary', $this->data);
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
		}
	}	

	/**
	 * Affiche le récapitulatif de l'utilisateur
	 * demandé
	 * @param String $user l'identifiant de l'utilisateur
	 */
	public function showUserRecap(){
		if($this->input->post('recap') != "")
		{
			if($this->user_connected())
			{
				if($this->session->userdata('login') == $this->input->post('recap'))
				{
					redirect('summary');
				}
			}

			$this->add_data($this->getUserRecap($this->input->post('recap')));
			$this->load->view('summary', $this->data);
		}
		else
		{
			$this->error_message("Cette page n'existe pas");
			$this->error_show('error_404');
		}
	}

	/**
	 * Récupère les données à afficher
	 * pour le récapitulatif d'un utilisateur
	 * @param String $user l'identifiant de l'utilisateur
	 * @return Array les données
	 */
	private function getUserRecap($user){
		$data = array();

		// Liste des modules et parties associées
		// Format : array (size=3)
  		// 'ALGOC1' => 
  		//   array (size=2)
  		//     'infos' => 
  		//       array (size=10)
  		//         'ident' => string 'ALGOC1' (length=6)
		//			...
  		//     'parts' => 
  		//       array (size=3)
  		//         0 => 
  		//           array (size=5)
  		//             ...
  		//         1 => 
  		//           array (size=5)
  		//             ...
  		// 'ALGOC2' => 
  		//   array (size=2)
  		//     'infos' => 
		$data['modules'] = array();

		$modules_list = $this->module->get_user_modules($user);

		foreach ($modules_list as $module) 
		{
			$data['modules'][$module['ident']]['infos'] = $module;

			$content = $this->contenu->get_user_module_contenu($user, $module['ident']);

			$data['modules'][$module['ident']]['parts'] = $content;
		}

		/* -- Récupération des données enseignant(s) -- */

		$data['teacher_info'] = $this->user->get_user_data($user);

		$data['teachers_list'] = $this->user->get_users_data();

		/* -- Récupération des statistiques -- */
		
		// Statistiques des modules
		$data['stats']['modules'] = $this->module->get_user_module_stats($user);

		// Statistiques parties (cours)
		$data['stats']['parts'] = $this->contenu->get_user_part_stats($user);
		
		// Statistiques sur les public enseignés
		$data['stats']['public_stats'] = $this->module->get_user_modules_stat_public($user);
		$temp = draw_graph($data['stats']['public_stats']);
		$data['stats']['images']['public'] = $temp['graph'];
		$data['stats']['legends']['public'] = $temp['legend'];

		// Statistiques sur les type de cours enseignés
		$data['stats']['type_stats'] = $this->contenu->get_user_modules_stat_type($user);
		$temp = draw_graph($data['stats']['type_stats'], 150, 150, 5);
		$data['stats']['images']['type'] = $temp['graph'];
		$data['stats']['legends']['type'] = $temp['legend'];

		return $data;
	}

	// Gère la désinscription d'un utilisateur à un module
	public function unsubscribe($userId, $module, $module_part){
		if(!$this->user_connected())
		{
			$this->error_message("Vous devez vous connecter afin de vous désinscrire d'une partie de module");
			$this->error_show('error_allowed');
		}
		else if($userId != $this->session->userdata('login'))
		{
			$this->error_message("Vous ne pouvez pas désinscrire quelqu'un d'autre que vous-même");
			$this->error_show('error_allowed');
		}
		else if(!$this->module->module_part_exists($module, rawurldecode($module_part)))
		{
			$this->error_message("Le module ou la partie de module n'existe pas");
			$this->error_show('error_allowed');
		}
		else
		{
			$this->contenu->unsubscribe($userId, $module, $module_part);
			$this->session->set_flashdata('success_message', 'Votre désinscription a été prise en compte');
			redirect('summary');
		}
	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */