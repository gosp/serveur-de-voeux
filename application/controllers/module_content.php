<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class module_content extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("contenu");
		$this->load->model("module");
	}


	/**
	 * Recherche un module
	 */
	public function searchModule()
	{
		if(!$this->session->userdata('connected')){
			$this->error_message("Vous devez vous connecter afin de consulter les informations d'un module");
			$this->error_show('error_allowed');
		}
		else if(!isset($_POST['nomModule'])){
			$this->error_message("Le module recherché n'existe pas");
			$this->error_show('error_allowed');
		}
		else{
			$this->index($_POST['nomModule']);
		}
	}

	
	/**
	 * Affiche les informations d'un module
	 * @param String $module l'identifiant du module consulté
	 */
	public function index($module = "ALGOC1")
	{	
		if(!$this->session->userdata('connected'))
		{
			$this->error_message("Vous devez vous connecter afin de consulter les informations d'un module");
			$this->error_show('error_allowed');
		}
		else if(!($this->module->module_exists($module))){
			$this->error_message("Le module recherché n'existe pas");
			$this->error_show('error_allowed');
		}
		else
		{
			$this->data['module_content'] = $this->contenu->get_module_contenu($module);
			$this->data['module_information'] = $this->module->get_module_information($module);
			$this->data['module_hours'] = $this->contenu->get_module_hours($module);
			$this->data['listeModules'] = $this->module->get_all_modules();
			$this->data['user_info'] = $this->session->all_userdata();

			//permet de mettre un lien vers l'inscription/la désinscription de l'utilisateur à un module
			foreach ($this->data['module_content'] as &$module) 
			{
				// Label enseignant
				if(is_null($module["enseignant"]))
				{
					$module["owner"] = "Libre";
				}
				else
				{
					$module["owner"] = $module["prenom"]. " ". $module["nom"];
				}

				if($this->user_connected())
				{
					// Lien pour s'inscrire
					$module["subscribeLink"] = "/module_content/subscribe/".
					$this->data['user_info']['login']."/".$module["module"]."/".$module["partie"];
					// Lien pour se désisncrire
					$module["unsubscribeLink"] = "/module_content/unsubscribe/".
					$this->data['user_info']['login']."/".$module["module"]."/".$module["partie"];
				}
			}
			$this->load->view('module_content', $this->data);
		}
	}

	
	/**
	 * Inscription d'un utilisateur à un module
	 * @param String $userId l'identifiant de l'utilisateur
	 * @param String $module l'identifiant du module auquel l'utilisateur s'inscrit
	 * @param String $module_part l'identifiant de la partie de module
	 */
	public function subscribe($userId, $module, $module_part){
		//Insérer l'enseignant dans contenu['enseignant']
		if(!$this->user_connected()){
			$this->error_message("Vous devez vous connecter afin de vous inscrire à une partie de module");
			$this->error_show('error_allowed');
		}else if($userId != $this->session->userdata('login')){
			$this->error_message("Vous ne pouvez pas inscrire quelqu'un d'autre que vous-même");
			$this->error_show('error_allowed');
		}else if(!$this->module->module_part_exists($module, rawurldecode($module_part))){
			$this->error_message("Le module ou la partie de module n'existe pas");
			$this->error_show('error_allowed');
		}else if(!$this->session->userdata('actif'))
		{
			$this->error_message("Vous devez être actif pour réserver un module");
			$this->error_show('error_allowed');
		}
		else
		{
			$this->contenu->subscribe($userId, $module, $module_part);
			$this->session->set_flashdata('success_message', 'Votre inscription a été prise en compte.');
			redirect('module_content/index/'.$module);
		}
	}

	
	/**
	 * Désnscription d'un utilisateur d'un module
	 * @param String $userId l'identifiant de l'utilisateur
	 * @param String $module l'identifiant du module auquel l'utilisateur se désinscrit
	 * @param String $module_part l'identifiant de la partie de module
	 */
	public function unsubscribe($userId, $module, $module_part){
		if(!$this->user_connected()){
			$this->error_message("Vous devez vous connecter afin de vous désinscrire d'une partie de module");
			$this->error_show('error_allowed');
		}else if($userId != $this->session->userdata('login')){
			$this->error_message("Vous ne pouvez pas désinscrire quelqu'un d'autre que vous-même");
			$this->error_show('error_allowed');
		}else if(!$this->module->module_part_exists($module, rawurldecode($module_part))){
			$this->error_message("Le module ou la partie de module n'existe pas");
			$this->error_show('error_allowed');
		}else{
			$this->contenu->unsubscribe($userId, $module, $module_part);
			$this->session->set_flashdata('success_message', 'Votre désinscription a été prise en compte.');
			redirect('module_content/index/'.$module);
		}
	}
}
