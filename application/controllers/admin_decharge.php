<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require_once(APPPATH.'controllers/base_controller.php');

class Admin_decharge extends Base_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->model ( "module" );
		$this->load->model ( "contenu" );
		$this->load->model ( "user" );
		$this->load->model ( "admin" );
		$this->load->model ( "decharge" );

	}

	/**
	 * Vérifie les droits d'accès aux pages
	 * et charge les pages d'erreur en cas de besoin
	 * @return Boolean
	 */
	private function checkAccess(){
		if($this->user_connected())
		{
			if(!$this->user_rights('administrateur'))
			{
				$this->error_message("Vous devez être administrateur pour consulter cette page");
				$this->error_show('error_allowed');
				return false;
			}
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
			return false;
		}
		return true;
	}

	/**
	 * Edition d'une decharge
	 */
	public function editDecharge(){
		if($this->checkAccess())
		{
			$enseignant = $this->input->post('enseignant');

			// Demande d'édition
			if( $this->input->post('editDecharge') == 1)
			{
				$this->data['editDecharge']['state'] = 1;
				$this->data['editDecharge']['data'] = $this->admin->get_info_data('decharge', 'enseignant', $enseignant);
				$this->set_alert('info_message', 'édition de '.$enseignant.' en cours..');
				$this->index();
			}
			else // modification
			{
				$this->form_validation->set_rules('enseignant', 'Enseignant', 'required');
				$this->form_validation->set_rules('decharge', 'Decharge', 'required');
				if ($this->form_validation->run() == TRUE)
				{
					$enseignant = $this->input->post('enseignant');
					$decharge = $this->input->post('decharge');
					$this->admin->editDecharge($enseignant,$decharge);
					$this->session->set_flashdata('success_message', 'Décharge modifiée avec succès');	
					redirect('/admin_decharge');
				}
				else
				{
					$this->data['editDecharge']['state'] = 1;
					$this->data['editDecharge']['data'] = $this->admin->get_info_data('decharge', 'enseignant', $enseignant);
					$this->set_alert('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs de modification');
					$this->index();
				}
			}
		}
	}

	/**
	 * Ajoute une décharge
	 */
	public function addDecharge(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('enseignant', 'Enseignant', 'required');
			$this->form_validation->set_rules('decharge', 'Decharge', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$enseignant = $this->input->post('enseignant');
				$decharge = $this->input->post('decharge');
				if(!$this->decharge->exists($enseignant))
				{
					$this->admin->addDecharge($enseignant,$decharge);
					$this->session->set_flashdata('success_message', 'La décharge a été créée avec succès');
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Cet enseignant a déjà une décharge qui lui est attribué');
				}
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs d\'ajout de décharge');
			}
			redirect('/admin_decharge');
		}
	}

	/**
	 * Supprime une décharge
	 */
	public function deleteDecharge(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('enseignant', 'Enseignant', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$enseignant = $this->input->post('enseignant');
				$this->admin->deleteDecharge($enseignant);
				$this->session->set_flashdata('success_message', 'La décharge a été supprimée avec succès');
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Vous n\'avez pas spécifié la décharge que vous souhaitiez supprimer');
			}
			redirect('/admin_decharge');
		}
	}

	/**
	 * Recherche des décharges
	 */
	public function searchDecharge(){
		if($this->checkAccess())
		{		
			$enseignant = $this->input->post('enseignant');
			
			$this->data ['decharges'] = $this->admin->searchDecharge ($enseignant);
			$this->data ['enseignants_login'] = $this->admin->get_all_teacher_login ();
			$this->data ['enseignants_add'] = $this->decharge->get_teachers_no_decharge();
			$this->data ['decharges_info'] = $this->admin->get_prof_decharge ('decharge');
			
			if(count($this->data['decharges']) == 0){
				$this->set_alert('info_message', 'Aucun résultat n\'a été trouvé');
			}else if(count($this->data['decharges']) == 1)
			{
				$this->set_alert('info_message', '1 résultat a été trouvé');
			}else{
				$this->set_alert('info_message', count($this->data['decharges']).' résultats ont été trouvés');
			}

			$this->load->view ( 'admin_decharge', $this->data );
		}
	}
	
	// Affiche la page administrateur de gestion des décharges
	public function index(){
		if($this->checkAccess())
		{
			$this->data ['decharges'] = $this->admin->get_table_data ('decharge');
			$this->data ['enseignants_login'] = $this->admin->get_all_teacher_login();
			$this->data ['enseignants_add'] = $this->decharge->get_teachers_no_decharge();
			$this->data ['decharges_info'] = $this->admin->get_prof_decharge ('decharge');

			$this->load->view ( 'admin_decharge', $this->data );
		}
	}
}
