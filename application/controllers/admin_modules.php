<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require_once(APPPATH.'controllers/base_controller.php');

class Admin_modules extends Base_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->model ( "module" );
		$this->load->model ( "contenu" );
		$this->load->model ( "user" );
		$this->load->model ( "admin" ); 
	}

	/**
	 * Vérifie les droits d'accès aux pages
	 * et charge les pages d'erreur en cas de besoin
	 * @return Boolean
	 */
	private function checkAccess(){
		if($this->user_connected())
		{
			if(!$this->user_rights('administrateur'))
			{
				$this->error_message("Vous devez être administrateur pour consulter cette page");
				$this->error_show('error_allowed');
				return false;
			}
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
			return false;
		}

		return true;
	}

	/**
	 * Ajouter un module
	 */
	public function addModule(){
		if($this->checkAccess())
		{	
			$this->form_validation->set_rules('ident', 'Ident', 'required');
			$this->form_validation->set_rules('public', 'Public', 'required');
			$this->form_validation->set_rules('semestre', 'Semestre', 'required');
			$this->form_validation->set_rules('libelle', 'Libelle', 'required');
			$this->form_validation->set_rules('responsable', 'Responsable', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$ident = $this->input->post('ident');
				$public = $this->input->post('public');
				$semestre = $this->input->post('semestre');
				$libelle = $this->input->post('libelle');
				$responsable = $this->input->post('responsable');

				if(!$this->module->module_exists($ident))
				{
					$this->admin->addModule($ident, $public, $semestre, $libelle, $responsable);
					$this->session->set_flashdata('success_message', 'Module '. $libelle .' ajouté avec succès');
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Module '. $libelle .' existe déjà, veuillez reneigner un autre module');
				}				
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs');
			}
			redirect('/admin_modules');
		}
	}
	
	/**
	 * Editer un module
	 */
	public function editModule(){
		if($this->checkAccess())
		{
			$ident = $this->input->post('ident');

			// Demande d'édition
			if( $this->input->post('editModule') == 1)
			{
				$this->data['editModule']['state'] = 1;
				$this->data['editModule']['data'] = $this->admin->get_info_data('module', 'ident', $ident);
				$this->set_alert('info_message', 'édition de '.$ident.' en cours..');
				$this->index();
			}
			else // modification d'un module
			{
				$this->form_validation->set_rules('ident', 'Ident', 'required');
				$this->form_validation->set_rules('public', 'Public', 'required');
				$this->form_validation->set_rules('semestre', 'Semestre', 'required');
				$this->form_validation->set_rules('libelle', 'Libelle', 'required');
				$this->form_validation->set_rules('responsable', 'Responsable', 'required');
				$this->form_validation->set_rules('responsable', 'Responsable', 'required');

				if ($this->form_validation->run() == TRUE)
				{
					$ident = $this->input->post('ident');
					$public = $this->input->post('public');
					$semestre = $this->input->post('semestre');
					$libelle = $this->input->post('libelle');
					$responsable = $this->input->post('responsable');
					$this->admin->editModule($ident,$public,$semestre,$libelle,$responsable);
					$this->session->set_flashdata('success_message', 'Module '. $libelle .' modifié avec succès');	

					redirect('/admin_modules');
				}
				else
				{
					$this->data['editModule']['state'] = 1;
					$this->data['editModule']['data'] = $this->admin->get_info_data('module', 'ident', $ident);
					$this->set_alert('error_message', 'Veuillez vérifier que tous les champs de modification du module sont bien remplie');	
					$this->index();
				}
			}
		}
	}


	/**
	 * Supprimer un module
	 */
	public function deleteModule(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('ident', 'Ident', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$ident = $this->input->post('ident');

				if($this->module->module_exists($ident))
				{
					$this->admin->deleteModule($ident);
					$this->session->set_flashdata('success_message', 'Module '. $libelle .' supprimé avec succès');
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Ce module n\'existe pas');
				}	
			}
			redirect('/admin_modules');
		}
	}
	
	/**
	 * Rechercher des modules
	 */
	public function searchModule(){	
		if($this->checkAccess())
		{
			$ident = $this->input->post('ident');
			$public = $this->input->post('public');
			$semestre = $this->input->post('semestre');

			$this->data ['modules_info'] = $this->admin->searchModule ($ident,$public,$semestre);
			$this->data ['public_list'] = $this->admin->get_public_list();
			$this->data ['modules'] = $this->admin->get_all_modules ();
			$this->data ['enseignants'] = $this->admin->get_table_data ('enseignant');
			
			if(count($this->data['modules_info']) == 0){
				$this->set_alert('info_message', 'Aucun résultat n\'a été trouvé');
			}else if(count($this->data['modules_info']) == 1)
			{
				$this->set_alert('info_message', '1 résultat a été trouvé');
			}else{
				$this->set_alert('info_message', count($this->data['modules_info']).' résultats ont été trouvés');
			}
		
			$this->load->view ('/admin_modules', $this->data);
		}
	}
	
	// Affiche la page administrateur de gestion des modules
	public function index(){
		if($this->checkAccess())
		{
			$this->data ['modules'] = $this->admin->get_all_modules ();
			$this->data ['public_list'] = $this->admin->get_public_list();
			$this->data ['modules_info'] = $this->admin->get_table_data ('module');
			$this->data ['enseignants'] = $this->admin->get_table_data ('enseignant');
			$this->load->view ( 'admin_modules', $this->data );
		}
	}
}
