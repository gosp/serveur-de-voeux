<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require_once(APPPATH.'controllers/base_controller.php');

class Admin_export extends Base_Controller {

	public function __construct(){
		parent::__construct ();
		$this->load->model("export");
		$this->load->helper('download');
	}

	/**
	 * Vérifie les droits d'accès aux pages
	 * et charge les pages d'erreur en cas de besoin
	 * @return Boolean
	 */
	private function checkAccess(){
		if($this->user_connected())
		{
			if(!$this->user_rights('administrateur'))
			{
				$this->error_message("Vous devez être administrateur pour consulter cette page");
				$this->error_show('error_allowed');
				return false;
			}
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
			return false;
		}
		return true;
	}

	
	// Affiche la page de l'exportation
	public function index(){
		if($this->checkAccess())
		{
			// Visualiser l'export
			if($this->param_exists('preview'))
			{
				if($this->param_exists('module'))
				{
					$this->data ['modules'] = $this->export->module_csv();
				}
				if($this->param_exists('enseignant'))
				{
					$this->data ['enseignants'] = $this->export->enseignant_csv();
				}
				if($this->param_exists('decharge'))
				{
					$this->data ['decharges'] = $this->export->decharge_csv();
				}				
				if($this->param_exists('affectation'))
				{
					$this->data ['affectations'] = $this->export->affectation_csv();
				}

				$this->set_alert('success_message', 'Visualisation effectuée avec succès');
			}

			// Télécharger l'export
			else if($this->param_exists('download'))
			{
				// Données à envoyer
				$data = "";

				// Nom du fichier qui sera téléchargé
				$name = "serveur-de-voeux";

				if($this->param_exists('module'))
				{
					$data .= $this->export->module_csv()."\r\n";
					$name .= "_modules";
				}
				if($this->param_exists('enseignant'))
				{
					$data .= $this->export->enseignant_csv()."\r\n";
					$name .= "_enseignants";
				}
				if($this->param_exists('decharge'))
				{
					$data .= $this->export->decharge_csv()."\r\n";
					$name .= "_decharges";
				}				
				if($this->param_exists('affectation'))
				{
					$data .= $this->export->affectation_csv()."\r\n";
					$name .= "_affectations";
				}

				$name .= ".csv";

				$this->set_alert('success_message', 'Téléchargement réussi');

				force_download($name, $data);
			}
			$this->load->view('admin_export', $this->data);			
		}
	}

}