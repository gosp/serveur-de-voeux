<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require_once(APPPATH.'controllers/base_controller.php');

class Administration extends Base_Controller {

	public function __construct() {

		parent::__construct ();
		$this->load->model ( "module" );
		$this->load->model ( "contenu" );
		$this->load->model ( "user" );
		$this->load->model ( "admin" );
	}

	/**
	 * Vérifie les droits d'accès aux pages
	 * et charge les pages d'erreur en cas de besoin
	 * @return Boolean
	 */
	private function checkAccess(){
		if($this->user_connected())
		{
			if(!$this->user_rights('administrateur'))
			{
				$this->error_message("Vous devez être administrateur pour consulter cette page");
				$this->error_show('error_allowed');
				return false;
			}
		}
		else
		{
			$this->error_message("Vous devez être connecté pour consulter cette page");
			$this->error_show('error_allowed');
			return false;
		}
		return true;
	}

	/**
	 * Ajouter un enseignant
	 */
	public function addProf(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('login', 'Login', 'required');
			$this->form_validation->set_rules('pwd', 'Password', 'required');
			$this->form_validation->set_rules('prenom', 'Prenom', 'required');
			$this->form_validation->set_rules('statut', 'Statut', 'required');
			$this->form_validation->set_rules('statutaire', 'Statutaire', 'required');
			$this->form_validation->set_rules('administrateur', 'Administrateur', 'required');
			$this->form_validation->set_rules('actif', 'Actif', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$login = $this->input->post('login');
				$pwd = $this->input->post('pwd');
				$nom = $this->input->post('nom');
				$prenom = $this->input->post('prenom');
				$statut = $this->input->post('statut');
				$statutaire = $this->input->post('statutaire');
				$actif = $this->input->post('actif');
				$administrateur = $this->input->post('administrateur');
				$this->session->set_flashdata('success_message', 'Enseignant ajouté avec succès');
				$this->admin->addProf($login,$pwd,$nom,$prenom,$statut,$statutaire,$actif,$administrateur);
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Veuillez vous assurer d\'avoir rempli chacun des champs');
			}

			redirect('/administration');
		}
	}
	
	/**
	 * Editer un enseignant
	 */
	public function editProf(){
		if($this->checkAccess())
		{
			$login = $this->input->post('login');

			// Demande d'édition
			if( $this->input->post('editProf') == 1)
			{
				$this->data['editProf']['state'] = 1;
				$this->data['editProf']['data'] = $this->admin->get_info_data('enseignant','login',$login);
				$this->set_alert('info_message', 'édition de '.$login.' en cours..');
				$this->index();
			}
			else // modification d'un enseignant
			{
				$this->form_validation->set_rules('pwd', 'Password', 'required');
				$this->form_validation->set_rules('prenom', 'Prenom', 'required');
				$this->form_validation->set_rules('nom', 'Nom', 'required');
				$this->form_validation->set_rules('statut', 'Statut', 'required');
				$this->form_validation->set_rules('statutaire', 'Statutaire', 'required');
				$this->form_validation->set_rules('administrateur', 'Administrateur', 'required');
				$this->form_validation->set_rules('actif', 'Actif', 'required');

				if ($this->form_validation->run() == TRUE)
				{
					$pwd = $this->input->post('pwd');
					$nom = $this->input->post('nom');
					$prenom = $this->input->post('prenom');
					$statut = $this->input->post('statut');
					$statutaire = $this->input->post('statutaire');
					$actif = $this->input->post('actif');
					$administrateur = $this->input->post('administrateur');
					$this->admin->editProf($login,$pwd,$nom,$prenom,$statut,$statutaire,$actif,$administrateur);
					$this->session->set_flashdata('success_message', 'Enseignant modifié avec succès');	
					redirect('/administration');
				}
				else
				{
					$this->data['editProf']['state'] = 1;
					$this->data['editProf']['data'] = $this->admin->get_info_data('enseignant','login',$login);
					$this->set_alert('error_message', 'La modification n\'a pas été prise en compte, tous les champs n\'ont pas été renseignés');
					$this->index();
				}
			}
		}
	}
	
	/**
	 * Supprimer un enseignant
	 */
	public function deleteProf(){
		if($this->checkAccess())
		{
			$this->form_validation->set_rules('login', 'Login', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$login = $this->input->post('login');
				$this->admin->deleteProf($login);
				$this->session->set_flashdata('success_message', 'Enseignant supprimé avec succès');	
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Vous n\'avez pas spécifié l\'enseignant que vous souhaitiez supprimer');
			}
			redirect('/administration');
		}
	}

	/**
	 * Rechercher des enseignants
	 */
	public function searchProf(){
		if($this->checkAccess())
		{
			$login = $this->input->post('login');
			$nom = $this->input->post('nom');
			$prenom = $this->input->post('prenom');
			$statut = $this->input->post('statut');
			
			$this->data['prof_info'] = $this->admin->searchProf ($login,$nom,$prenom,$statut);
			$this->data['enseignants'] = $this->admin->get_table_data ('enseignant');
			
			if(count($this->data['prof_info']) == 0){
				$this->set_alert('info_message', 'Aucun résultat n\'a été trouvé');
			}else if(count($this->data['prof_info']) == 1)
			{
				$this->set_alert('info_message', '1 résultat a été trouvé');
			}else{
				$this->set_alert('info_message', count($this->data['prof_info']).' résultats ont été trouvés');
			}
			$this->load->view ( 'administration', $this->data );
		}
	}
	
	// Affiche la page administrateur de gestion des enseignants
	public function index() {

		if($this->checkAccess())
		{
			$this->data ['course_info'] = $this->admin->get_table_data ('contenu');
			$this->data ['prof_info'] = $this->admin->get_table_data ('enseignant');
			$this->data ['modules'] = $this->admin->get_all_modules ();
			$this->data ['enseignants'] = $this->admin->get_table_data ('enseignant');
			$this->load->view ( 'administration', $this->data );			
		}
	}
}