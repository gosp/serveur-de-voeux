<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Login extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("user");
	}

	// Affiche la page de login
	public function index()
	{
		if($this->session->userdata('connected'))
		{
			redirect('/home', 'location');
		}
		else
		{
			$this->load->view("login");
		}
	}	

	/**
	 * Connecte un utilisateur
	 */
	public function connection(){

		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('login', 'Username', 'required|callback_login_check');

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('error_message', 'Identifiants incorrectes');

			// redirection page login
			redirect('/login');
		}
		else
		{
			// On met en session les données de l'utilisateur connecté
			$user_data = $this->user->get_user_data($this->input->post('login'));
			$this->session->set_userdata($user_data);
			$this->session->set_userdata(array("connected" => true));

			$this->session->set_flashdata('success_message', 'Vous êtes connecté');

			// redirection page home
			redirect('/home');
		}
	}

	/**
	 * Vérifie les identifiants de l'utilisateur
	 * @return Boolean
	 */
	public function login_check()
	{
		return $this->user->check_login(
			$this->input->post('login'), 
			$this->input->post('password')
		);
	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */