<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base_controller.php');

class Main extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("module");
	}

	// Affiche la page main
	public function index($sort = 'libelle')
	{
		if(!$this->session->userdata('connected'))
		{
			$this->error_message("Vous devez vous connecter afin de consulter les différents modules");
			$this->error_show('error_allowed');
		}
		else
		{
			if($sort == "libelle" || $sort == "public" || $sort == "semestre")
			{
				$this->data['modules'] = $this->module->get_all_sort_modules($sort);
				foreach ($this->data['modules'] as &$module) 
				{
					$module['pourcentage'] = round($this->module->get_module_completed_pourcentage($module['ident']));
				}

				$this->data['sort'] = $sort;
				
				$this->load->view('main', $this->data);
			}
			else
			{
				$this->error_message("Cette page n'existe pas");
				$this->error_show('error_404');
			}

		}
	}	

	/**
	 * Déconnecte l'utilisateur
	 */
	public function disconnect()
	{
		if($this->session->userdata('connected'))
		{
			$this->session->sess_destroy();
			$this->session->set_flashdata('success_message', 'Déconnecté');
			redirect('/login');
		}
		else
		{
			$this->session->set_flashdata('error_message', "Vous n'êtes pas connecté");
			redirect('/main');
		}


	}
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */