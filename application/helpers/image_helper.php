<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dessine une image (graphique) à partir des données fournies
 * Et présente également la légende associée
 * @param Array $data
 *        format :
 *        array(size=x)
 *        	0 => 
 *		  	  array (size=4)
 *		  	    'module' => string 'ALGOC1' (length=6)
 *		  	    'public' => string 'TC' (length=2)
 *		  	    'label' => String 'TC' (length=2)
 *		  	    'nb' => string '2' (length=1)
 *		  	    'poucentage' => float 28.571428571429
 *		  	1 => 
 *		  	  array (size=4)
 *		  	    'module' => string 'ALGOC2' (length=6)
 *		  	    'public' => string 'LSI1' (length=4)
 *		  	    'label' => string 'LSI1' (length=4)
 *		  	    'nb' => string '2' (length=1)
 *		  	    'poucentage' => float 28.571428571429'
 *		  	2 ...
 *
 * @return Array("graph" => image_du_graph, "legend" => array(array("label" => "TC (50%)", img => "image_associée"), array(...)))
 */
if ( ! function_exists('draw_graph'))
{
	function draw_graph(array $data, $width = 150, $heigh = 150, $color_start_index = 1)
	{

		// Liste des légendes
		$legend = array();

		$grah = imagecreatetruecolor(200, 200);

		$colors = array(
			0 => imagecolorallocate($grah, 0, 0, 0), // noir
			1 => imageColorAllocate($grah, 255, 0, 0), // rouge
			2 => imageColorAllocate($grah, 0, 255, 0),  // vert
			3 => imageColorAllocate($grah, 0, 0, 255), // bleu
			4 => imageColorAllocate($grah, 255, 255, 0), // jaune
			5 => imageColorAllocate($grah, 255, 140, 0), // orange
			6 => imageColorAllocate($grah, 255, 20, 147), // rose
			7 => imageColorAllocate($grah, 160, 32, 240), // violet
			8 => imageColorAllocate($grah, 0, 255, 255), // cyan
			9 => imageColorAllocate($grah, 110, 139, 61), // darkolive
			10 => imageColorAllocate($grah, 169, 169, 169) // gray
			// 11 => imageColorAllocate($grah, 0, 255, 0), 
			// 12 => imageColorAllocate($grah, 0, 0, 153), 
			// 13 => imageColorAllocate($grah, 153, 0, 153), 
			// 14 => imageColorAllocate($grah, 153, 0, 0), 
			// 15 => imageColorAllocate($grah, 153, 153, 0) 
		);

		imagecolortransparent($grah, $colors[0]);

		$center_x = 100;
		$center_y = 100;

		$angle_start = 0;
		$angle_end = 0;
		$color = $color_start_index;

		if(count($data) <= 0)
		{
			imagefilledarc($grah, $center_x, $center_y, $width, $heigh, 0, 360, $colors[10] , IMG_ARC_PIE);
		}
		else
		{
			foreach ($data as &$val) 
			{
				$angle_start = $angle_end;
				$angle_end = $angle_start + ($val['pourcentage']* 3.6) ;
				$color++;

				$leg = imagecreatetruecolor(20, 20);

				imagefilledrectangle($leg, 0, 0, 20, 20, $colors[$color]);

				ob_start();

        		imagepng($leg, NULL, 0, NULL);

        		$current_legend = ob_get_clean();

        		array_push($legend, array(
        			"label" => $val['label']. " (".round($val['pourcentage'])."%)",
        			"img" => $current_legend));

				// Pour le débug : 
				// echo 'imagefilledarc($grah, '.$center_x.', '.$center_y.', '.$width.', '.$heigh.', '.$angle_start.', '.$angle_end.', '.$colors[$color].' , IMG_ARC_PIE)</br>';

				imagefilledarc($grah, $center_x, $center_y, $width, $heigh, $angle_start, $angle_end, $colors[$color] , IMG_ARC_PIE);
			}			
		}

        ob_start();

        imagepng($grah, NULL, 0, NULL);

        $data["graph"] = ob_get_clean();
        $data['legend'] = $legend;

        return $data;
	}
}
