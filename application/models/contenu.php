<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle gère le contenu des modules, c'est à dire les cours
 * CM, TD, TP ainsi que les affectations, et sélections
 */
class Contenu extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Récupère les contenu associés à l'enseignant
     * @param  String $login
     * @param  String $module
     * @return Array liste parties des modules (contenu)
     */
    public function get_user_module_contenu($login, $module)
    {
        $this->db->select('*');
        $this->db->from('contenu');
        $this->db->join('module', 'module.ident = contenu.module');
        $this->db->where('enseignant', $login);
        $this->db->where('module', $module);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère les parties des différents modules
     * @return array
     */
    public function get_all_parts()
    {
        $this->db->select('DISTINCT(partie) AS partie');
        $this->db->from('contenu');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }    
    }

    /**
     * Sélectionne le contenu associé à une partie d'un module
     * @param  String $module 
     * @param  String $partie 
     * @return Array
     */
    public function get_module_part_content($module, $partie)
    {
        $this->db->select('*');
        $this->db->from('contenu');
        $this->db->where(array('module' => $module, 'partie' => $partie));
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère le contenu associé à un module
     * @param  String $module
     * @return Array list contenu module
     */
    public function get_module_contenu($module)
    {
        $this->db->select('*');
        $this->db->from('contenu');
        $this->db->join('enseignant', 'contenu.enseignant = enseignant.login', 'left outer');
        $this->db->where('module', $module);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère le nombre d'heure associé à un module
     * @param  String $module
     * @return Array list contenu module
     */
    public function get_module_hours($module){
        $this->db->select("(SELECT SUM(contenu.hed) FROM contenu WHERE contenu.module='$module') AS sum", FALSE);
        $query = $this->db->get();
        $sum_temp =  $query->row_array();
        $sum = $sum_temp['sum'];
        if(is_null($sum))
        {
            return 0;
        }
        else
        {
            return $sum;
        }
    }

    /**
     * Inscription d'un utilisateur à une partie de module
     * @param String $userId
     * @param String $module
     * @param String $module_part
    */
    public function subscribe($userId, $module, $module_part){
        //Insérer l'enseignant dans contenu['enseignant']
        $row = array('enseignant' => $userId);
        $this->db->where('module', urldecode($module));
        $this->db->where('partie', urldecode($module_part));
        $this->db->update('contenu', $row); 
    }

     /**
     * Désinscription d'un utilisateur à une partie de module
     * @param String $userId
     * @param String $module
     * @param String $module_part
    */
    public function unsubscribe($userId, $module, $module_part){
        //Supprimer l'enseignant de contenu['enseignant']
        $row = array('enseignant' => NULL);
        $this->db->where('module', urldecode($module));
        $this->db->where('partie', urldecode($module_part));
        $this->db->update('contenu', $row); 
    }

    /**
     * Calcule de pourcentage de participation d'un enseignant
     * à un un type de partie (CM, TD, TP)
     * @param  String $userId 
     * @return Array
     */
    public function get_user_modules_stat_type($userId)
    {
        // Compte le nombre total de parties affectés à l'enseignant $userId
        $total_count = $this->db->query("SELECT COUNT(*) as nb FROM contenu
            INNER JOIN module ON module.ident = contenu.module
            WHERE enseignant = ?", array($userId));

        $total_count = $total_count->row_array();
        $total_count = $total_count['nb'];

        // Compte le nombre de parties enseignées de type différent
        $type_count = $this->db->query("SELECT module, type, type AS label, COUNT(*) AS nb FROM `contenu`
            WHERE enseignant = ?
            GROUP BY contenu.type", array($userId));

        $type_count = $type_count->result_array();

        foreach ($type_count as &$part) 
        {
            $part['pourcentage'] = ($part['nb'] * 100) / $total_count;
        }

        return $type_count;
    }

    /**
     * Récupère les statistiques générale sur les parties(cours) d'un enseignant
     * @param  String $userId 
     * @return Array(part_affected_count => int, part_hour_count => int)
     */
    public function get_user_part_stats($userId)
    {
        $res = array();

        // Nombre de modules affectés
        $this->db->select('SUM( DISTINCT hed) AS nb');
        $this->db->from('contenu');
        $this->db->where('enseignant', $userId);
        $temp = $this->db->get()->row_array();
        if($temp['nb'] == null)
        {
            $res['part_hour_count'] = 0;
        }
        else
        {
            $res['part_hour_count'] = $temp['nb'];
        }
        
        // Nombre de modules dans lequel l'enseignant est responsable
        $this->db->select('*');
        $this->db->from('contenu');
        $this->db->where('enseignant', $userId);
        $res['part_affected_count'] = $this->db->count_all_results();

        return $res;
    }
}

?>