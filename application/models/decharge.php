<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle gère la décharge des enseignants
 * C'est-à-dire leur nombre d'heures de cours effectives
 * et celles en décharge
 */
class Decharge extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Retourne un booleen traduisant l'existence ou non de l'enseignant dans la table decharge
     * @param  String $enseignant
     * @return Boolean
     */
    public function exists($enseignant){
        $this->db->select('enseignant');
        $this->db->from('decharge');
        $this->db->where('enseignant', $enseignant);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Retourne les enseignants n'ayant pas de décharge attribuée
     * @return Array[]
     */
    public function get_teachers_no_decharge(){
        $query = $this->db->query("SELECT login FROM enseignant WHERE login NOT IN (SELECT enseignant FROM decharge);");

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
}

?>