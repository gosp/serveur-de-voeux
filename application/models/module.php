<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle gère les modules (ajouts, supression, modification, liste)
 */
class Module extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Sélectionne tous les modules de l'application
     * @return Array
     */
    public function get_all_modules()
    {
        $this->db->select('*');
        $this->db->from('module');
        $this->db->join('enseignant', 'module.responsable = enseignant.login', 'left outer');
        $this->db->order_by('public');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère la liste des modules trié selon
     * $sort
     * @param  String $sort type
     * @return Array les modules
     */
    public function get_all_sort_modules($sort)
    {
        $this->db->select('*');
        $this->db->from('module');
        $this->db->join('enseignant', 'module.responsable = enseignant.login', 'left outer');
        $this->db->order_by($sort);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère les modules associés à l'enseignant
     * @param  String $login
     * @return Array liste des modules
     */
    public function get_user_modules($login)
    {
        $this->db->select('*');
        $this->db->from('module');
        $this->db->join('contenu', 'module.ident = contenu.module', 'inner join');
        $this->db->where('contenu.enseignant', $login);
        $this->db->group_by("module");
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Calcule de pourcentage de complétion d'un module
     * donné (nombre d'affectations sur le total possible)
     * @param  String $module_id l'id du module
     * @return String|Boolean le pourcentage
     */
    public function get_module_completed_pourcentage($module_id)
    {
        $query = $this->db->query("SELECT (COUNT(*)*100/(SELECT COUNT(*) FROM contenu WHERE module=?)) AS pourcentage FROM contenu WHERE module=? AND enseignant IS NOT NULL", array($module_id, $module_id));

        if($query->num_rows() > 0)
        {
            $res = $query->row_array(0);
            return $res['pourcentage'];
        }
        else
        {
            return 0;
        }
        
    }

    /**
     * Récupère les informations associées à un module
     * @param  String $module
     * @return Array list contenu module
     */
    public function get_module_information($module){
        $this->db->select('*');
        $this->db->from('module');
        $this->db->join('enseignant', 'module.responsable = enseignant.login', 'left outer');
        $this->db->where('ident', $module);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Calcule de pourcentage de participation d'un enseignant
     * à un public (LSI, IMR, ...)
     * @param  String $userId 
     * @return Array
     */
    public function get_user_modules_stat_public($userId)
    {
        // Compte le nombre total de parties affectés à l'enseignant $userId
        $total_count = $this->db->query("SELECT COUNT(*) as nb FROM contenu
            INNER JOIN module ON module.ident = contenu.module
            WHERE enseignant = ?", array($userId));

        $total_count = $total_count->row_array();
        $total_count = $total_count['nb'];

        // Compte le nombre de de parties enseignées par public différent
        $modules_count = $this->db->query("SELECT module, public, public AS label, COUNT(*) AS nb FROM `contenu`
            INNER JOIN module ON module.ident = contenu.module
            WHERE enseignant = ?
            GROUP BY module.public", array($userId));

        $modules_count = $modules_count->result_array();

        foreach ($modules_count as &$module) 
        {
            $module['pourcentage'] = ($module['nb'] * 100) / $total_count;
        }

        return $modules_count;
    }

    /**
     * Récupère les statistiques générale sur les module d'un enseignant
     * @param  String $userId 
     * @return Array(module_affected_count => int, module_owner_count => int)
     */
    public function get_user_module_stats($userId)
    {
        $res = array();

        // Nombre de modules affectés
        $this->db->select('COUNT( DISTINCT ident) AS nb');
        $this->db->from('module');
        $this->db->join('contenu', 'module.ident = contenu.module', 'inner');
        $this->db->where('enseignant', $userId);
        $temp = $this->db->get()->row_array();
        $res['module_affected_count'] = $temp['nb'];
        
        // Nombre de modules dans lequel l'enseignant est responsable
        $this->db->select('*');
        $this->db->from('module');
        $this->db->where('responsable', $userId);
        $res['module_owner_count'] = $this->db->count_all_results();

        return $res;
    }

    /**
     * Test l'existence d'un module
     * @param String $idModule
     * @return boolean 
     */
    public function module_exists($idModule){
        $this->db->select('*');
        $this->db->from('module');
        $this->db->where("ident", $idModule);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Test l'existence d'une partie de module
     * @param String $idModule
     * @return boolean 
     */
    public function module_part_exists($idModule, $idPartModule){
        $this->db->select('*');
        $this->db->from('contenu');
        $this->db->where(array("module" => $idModule, "partie" => $idPartModule));
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>