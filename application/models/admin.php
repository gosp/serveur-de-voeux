<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce model gère les actions spécifiques aux administrateurs
 */
class Admin extends CI_Model {


    public function __construct()
    {
        parent::__construct();
    }	

    /**
     * Retourne les différents publics
     * @return array 
     */
    public function get_public_list()
    {
    	$this->db->select('DISTINCT(public)');
    	$this->db->from('module');
    	$query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $a = $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère les données d'un enseignant
     * @param String $userId
     * @return Array
     */
    public function get_info_data($table, $att, $moduleId)
    {
    	$this->db->select('*');
        $this->db->from($table);
        $this->db->where($att, $moduleId);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère le contenus des modules
     * @return array
     */
    public function get_contenu_module_data()
    {
    	$this->db->select('*');
        $this->db->from('contenu');
        $this->db->distinct('module');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return array();
        }
    }

	/**
	 * Afficher les données de la table passée en paramètre
	 * @param  String $table
	 * @return array
	 */
	public function get_table_data($table){
		$this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
	}

	/**
	 * Récupère l'attribut enseignant de la table fournie en paramètre
	 * @param  String table
	 * @return array
	 */
	public function get_prof_decharge($table){
		$this->db->select('enseignant');
        $this->db->from($table);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
	}	

	/**
	 * Récupère les informations des modules
	 * @return array
	 */
	public function get_all_modules()
	{
		$this->db->select('*');
		$this->db->from('module');
		$this->db->join('enseignant', 'module.responsable = enseignant.login', 'left outer');
		$this->db->order_by('public');
		$query = $this->db->get();
	
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Ajouter une affectation
	 * @param String $module
	 * @param String $partie
	 * @param String $type
	 * @param String $hed : heures d'enseignement
	 * @param String $enseignant
	 */
	public function addCourse($module,$partie,$type,$hed,$enseignant = "null"){
		if($enseignant == "null")
		{
			$data = array(
					'module' => $module,
					'partie' => $partie,
					'type' => $type,
					'hed' => $hed,
					'enseignant' => NULL
			);
		}
		else
		{
			$data = array(
					'module' => $module,
					'partie' => $partie,
					'type' => $type,
					'hed' => $hed,
					'enseignant' => $enseignant
			);
		}
		$this->db->insert('contenu', $data); 
	}

	/**
	 * Ajouter un enseignant
	 * @param String $login
	 * @param String $pwd
	 * @param String $nom
	 * @param String $prenom
	 * @param String $statut
	 * @param String $statutaire
	 * @param String $actif
	 * @param String $administrateur
	 */
	public function addProf($login,$pwd,$nom,$prenom,$statut,$statutaire,$actif,$administrateur){
		$data = array(
				'login' => $login,
				'pwd' => $pwd,
				'nom' => $nom,
				'prenom' => $prenom,
				'statut' => $statut,
				'statutaire' => $statutaire,
				'actif' => $actif,
				'administrateur' => $administrateur,
		);
		$this->db->insert('enseignant', $data); 
	}

	/**
	 * Ajouter une décharge
	 * @param String $enseignant
	 * @param String $decharge
	 */
	public function addDecharge($enseignant,$decharge){
		$data = array(
				'enseignant' => $enseignant,
				'decharge' => $decharge,
		);
		$this->db->insert('decharge', $data); 
	}

	/**
	 * Ajouter un module
	 * @param String $ident
	 * @param String $public
	 * @param String $semestre
	 * @param String $libelle
	 * @param String $responsable
	 */
	public function addModule($ident, $public, $semestre, $libelle, $responsable){
		if($responsable == "null")
		{
			$data = array(
					'ident' => $ident,
					'public' => $public,
					'semestre' => $semestre,
					'libelle' => $libelle,
					'responsable' => NULL,
			);
		}
		else
		{
			$data = array(
					'ident' => $ident,
					'public' => $public,
					'semestre' => $semestre,
					'libelle' => $libelle,
					'responsable' => $responsable,
			);			
		}

		$this->db->insert('module', $data); 
	}

	/**
	 * Modifier une affectation
	 * @param  String $module
	 * @param  String $partie
	 * @param  String $type
	 * @param  String $hed
	 * @param  String $enseignant
	 */
	public function editCourse($module,$partie,$type,$hed,$enseignant = 'null'){
		if($enseignant == "null")
		{
			$data = array(
				'module' => $module,
				'partie' => $partie,
				'type' => $type,
				'hed' => $hed,
				'enseignant' => null
			);	
		}
		else
		{
			$data = array(
					'module' => $module,
					'partie' => $partie,
					'type' => $type,
					'hed' => $hed,
					'enseignant' => $enseignant
			);		
		}

		$where = array(
				'module' => $module,
				'partie' => $partie,
		);
		
		$this->db->update('contenu', $data,$where); 
	}

	/**
	 * Modifier un enseignant
	 * @param  String $login
	 * @param  String $pwd
	 * @param  String $nom
	 * @param  String $prenom
	 * @param  String $statut
	 * @param  String $statutaire
	 * @param  String $actif
	 * @param  String $administrateur
	 */
	public function editProf($login,$pwd,$nom,$prenom,$statut,$statutaire,$actif,$administrateur){
		$data = array(
				'login' => $login,
				'pwd' => $pwd,
				'nom' => $nom,
				'prenom' => $prenom,
				'statut' => $statut,
				'statutaire' => $statutaire,
				'actif' => $actif,
				'administrateur' => $administrateur
		);
		$where = array(
				'login' => $login,
		);
		
		$this->db->update('enseignant', $data,$where); 
	}

	/**
	 * Modifier une décharge
	 * @param  String $enseignant
	 * @param  String $decharge
	 */
	public function editDecharge($enseignant,$decharge){
		$data = array(
				'enseignant' => $enseignant,
				'decharge' => $decharge,
		);
		$where = array(
				'enseignant' => $enseignant,
		);
		
		$this->db->update('decharge', $data,$where); 
	}

	/**
	 * Modifier un module
	 * @param  String $ident
	 * @param  String $public
	 * @param  String $semestre
	 * @param  String $libelle
	 * @param  String $responsable
	 */
	public function editModule($ident,$public,$semestre,$libelle,$responsable = "null"){
		if($responsable == "null")
		{
			$data = array(
					'public' => $public,
					'semestre' => $semestre,
					'libelle' => $libelle,
					'responsable' => NULL,
			);			
		}
		else
		{
			$data = array(
				'public' => $public,
				'semestre' => $semestre,
				'libelle' => $libelle,
				'responsable' => $responsable,
			);
		}

		$where = array(
				'ident' => $ident,
		);
		
		$this->db->update('module', $data, $where); 
	}

	/**
	 * Supprimer une affectation
	 * @param  String $module
	 * @param  String $partie
	 */
	public function deleteCourse($module,$partie){
		$condition = array(
				'module' => $module,
				'partie' => $partie,
		);
		$this->db->delete('contenu', $condition);
		
	}

	/**
	 * Supprimer un enseignant
	 * @param  String $login
	 */
	public function deleteProf($login){
		$condition = array(
				'login' => $login,
		);
		$this->db->delete('enseignant', $condition);	
	}

	/**
	 * Supprimer une décharge
	 * @param String $enseignant
	 */
	public function deleteDecharge($enseignant){
		$condition = array(
				'enseignant' => $enseignant,
		);
		$this->db->delete('decharge', $condition);	
	}

	/**
	 * Supprime un module
	 * @param String $ident
	 */
	public function deleteModule($ident){
		$condition = array(
				'ident' => $ident,
		);
		$this->db->delete('module', $condition);	
	}

	/**
	 * Rechercher des affectations
	 * @param  String $module
	 * @param  String $partie
	 * @param  String $type
	 * @param  String $enseignant
	 * @return array
	 */
	public function searchCourse($module,$partie,$type,$enseignant){
		$this->db->select('*');
		$this->db->from('contenu');
		if($module != "null"){
			$this->db->where('module', $module);
		}
		if($partie != "null"){
			$this->db->where('partie', $partie);
		}
		if($type != "null"){
			$this->db->where('type', $type);
		}
		if($enseignant != "null"){
			$this->db->where('enseignant', $enseignant);
		}
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

	/**
	 * Rechercher des enseignants
	 * @param  String $login
	 * @param  String $nom
	 * @param  String $prenom
	 * @param  String $statut
	 * @return array 
	 */
	public function searchProf ($login,$nom,$prenom,$statut){
		$this->db->select('*');
		$this->db->from('enseignant');
		if($login != ""){
			$this->db->where('login', $login);
		}
		if($nom != ""){
			$this->db->where('nom', $nom);
		}
		if($prenom != ""){
			$this->db->where('prenom', $prenom);
		}
		if($statut != ""){
			$this->db->where('statut', $statut);
		}
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

	/**
	 * Rechercher des décharges
	 * @param  String $enseignant
	 * @return array
	 */
	public function searchDecharge ($enseignant) {
		$this->db->select('*');
		$this->db->from('decharge');
		if($enseignant != ""){
			$this->db->where('enseignant', $enseignant);
		}
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

	/**
	 * Rechercher des modules
	 * @param  String $ident
	 * @param  String $public
	 * @param  String $semestre
	 * @return array
	 */
	public function searchModule ($ident,$public,$semestre) {
		$this->db->select('*');
		$this->db->from('module');
		if($ident != ""){
			$this->db->where('ident', $ident);
		}
		if($public != ""){
			$this->db->where('public', $public);
		}
		if($semestre != ""){
			$this->db->where('semestre', $semestre);
		}
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

	/**
	 * Récupérer les login
	 * @return array
	 */
	public function get_all_teacher_login()
	{
		$this->db->select('login');
		$this->db->from('enseignant');
		$query = $this->db->get();
	
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return array();
		}
	}

}
?>