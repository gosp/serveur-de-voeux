<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle gère les questions de la FAQ
*/
class Questions extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Récupère les questions de la FAQ dont le type est privé (pour utilisateur connecté)
     * @return Array list questions
     */
    public function get_all_questions()
    {
        $this->db->select('*');
        $this->db->from('questions');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère les questions de la FAQ dont le type est public
     * @return Array list questions
     */
    public function get_questions_not_connected()
    {
        $this->db->select('*');
        $this->db->from('questions');
        $this->db->where('questions.type', "public");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
}
?>