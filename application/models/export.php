<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle gère les export de données de la base de données
 */
class Export extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->dbutil();
    }
    
    /**
     * exporte les affectations au format csv
     * @return Array csv
     */
    public function affectation_csv()
    {
        $query = $this->db->query("SELECT * FROM contenu");

        return $this->dbutil->csv_from_result($query);
    }

    /**
     * exporte les enseignants au format csv
     * @return Array csv
     */
    public function enseignant_csv()
    {
        $query = $this->db->query("SELECT * FROM enseignant");

        return $this->dbutil->csv_from_result($query);
    }

    /**
     * exporte les modules au format csv
     * @return Array csv
     */
    public function module_csv()
    {
        $query = $this->db->query("SELECT * FROM module");

        return $this->dbutil->csv_from_result($query);
    }

    /**
     * exporte les decharges au format csv
     * @return Array csv
     */
    public function decharge_csv()
    {
        $query = $this->db->query("SELECT * FROM decharge");

        return $this->dbutil->csv_from_result($query);
    }

}

?>