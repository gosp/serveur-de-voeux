<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ce modèle rassemble les actions et données 
 * relatives à tous les utilisateurs
 */
class User extends CI_Model {


    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Vérifie les identifiants d'un utilisateur
     * @param  String $username 
     * @param  String $password 
     * @return Boolean
     */
    public function check_login($username, $password)
    {
        if($username == "" || $password == "") return false;

        $this->db->select('*');
        $this->db->from('enseignant');
        $this->db->where(array("login" => $username, "pwd" => $password));
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Récupère les données d'un utilisateur
     * @param String $username 
     * @return Array
     */
    public function get_user_data($username)
    {
        if($username == "") return array();

        $this->db->select('login, nom, prenom, statut, statutaire, actif, administrateur');
        $this->db->from('enseignant');
        $this->db->where(array("login" => $username));
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return array();
        }
    }

    /**
     * Récupère les données de tous les utilisateurs 
     * @return Array
     */
    public function get_users_data()
    {
        $this->db->select('*');
        $this->db->from('enseignant');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
}

?>