<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Serveur de voeux - erreur</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

        <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
            <div class="panel panel-default" style="padding:10px">
            <h3 class="sub-header" style="margin-top: 10px;">Erreur - Page non trouvée</h3>
                <table>
                    <tr>
                        <td>
                            <img src="/assets/images/error_64x64.png" alt="user avatar" style="margin-right:20px;">
                        </td>
                        <td>
                            Connectez vous <a href="/login">ici</a> ou revenez en  
                            <a href="<?php echo $_redirect_page; ?>">arrière</a>.
                        </td>
                    </tr>    
                </table>
                <br>
            </div>

        </div>
        <hr/>
      </div>
    </div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>