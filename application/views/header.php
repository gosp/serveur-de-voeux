    <div id="header" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <img src="/assets/images/site_icon_48x48.png" alt="icone" id="site-icon">
          <a class="navbar-brand" href="/home">Serveur de voeux</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
                <?php if($connected): ?>
                    <a href="/summary"><span class="glyphicon glyphicon-user"></span>
                      <?php echo $this->session->userdata('prenom')." ".$this->session->userdata('nom'); ?>
                    </a>
                <?php else: ?>
                    <a href="/login/login"><span class="glyphicon glyphicon-off"></span>  Se connecter</a>
                <?php endif; ?>
            </li>  
            <li>
                <?php if($connected): ?>
                    <a href="/main/disconnect">
                        <span class="glyphicon glyphicon-off"></span>  Se déconnecter</a>
                <?php endif; ?>
            </li>          
          </ul>
        </div>
      </div>
    </div>