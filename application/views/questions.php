<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Frequently Asked Questions</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">
        <div class="row">
            <!-- Menu de gauche -->
            <?php $this->load->view('menu-left.php'); ?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <!-- Messages d'alerte -->
                <?php $this->load->view('alert.php'); ?>

                <?php foreach($questions as $key=>$value): ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $value['titre'];?></h3>
                    </div>
                    <div class="panel-body">
                        <?php echo $value['reponse'];?>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>        
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>