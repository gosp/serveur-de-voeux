<!-- menu administration -->
<div id="menu-admin" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <ul id="ul-admin">

        <!-- Lien Récapitulatif -->
        <?php if($current_page == 'administration'): ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/administration"> Gérer les enseignants</a>
            <div class="coin"></div>
        </li>
        <?php else: ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/administration"> Gérer les enseignants</a>
        </li>
        <?php endif ?>

        <!-- Lien Récapitulatif -->
        <?php if($current_page == 'admin_decharge'): ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_decharge"> Gérer les décharges</a>
            <div class="coin"></div>
        </li>
        <?php else: ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_decharge"> Gérer les décharges</a>
        </li>
        <?php endif ?>

        <!-- Lien Récapitulatif -->
        <?php if($current_page == 'admin_modules'): ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_modules"> Gérer les modules</a>
            <div class="coin"></div>
        </li>
        <?php else: ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_modules"> Gérer les modules</a>
        </li>
        <?php endif ?>

        <!-- Lien Récapitulatif -->
        <?php if($current_page == 'admin_affectation'): ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_affectation"></span> Gérer l'affectation</a>
            <div class="coin"></div>
        </li>
        <?php else: ?>
        <li class="li-niveau1" style="position:relative;">
            <a href="/admin_affectation"> Gérer l'affectation</a>
        </li>
        <?php endif ?>

    </ul>
</div>