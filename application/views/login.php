<html>
	<title>Serveur de voeux - login</title>
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/login.css">
<body>
    <div class="container">
        
        <h4 class="form-signin-heading title">
            <a style="text-decoration:none; color:white" href="/home">
            <img src="/assets/images/site_icon_48x48.png" alt="icone" id="site-icon">  Serveur de voeux
            </a>
        </h4>
        
        <br>

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

        <!-- formulaire de connection -->
        <form class="form-signin" role="form" method="POST" action="/login/connection">
            <!-- identifiant -->
            <div class="input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
              <input type="text" name="login" class="form-control connect-field" placeholder="identifiant" required="" autofocus="">
            </div>
            <br>
            <!-- mot de passe -->
            <div class="input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
              <input type="password" name="password" class="form-control connect-field" placeholder="mot de passe" required="">
            </div>
            <br>
            <!-- valider -->
            <button id="button-connect" class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
        </form>
        <a style="color:white" href="/home">Ou continuer en mode visiteur ></a>
    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>