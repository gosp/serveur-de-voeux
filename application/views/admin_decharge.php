
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Administration - Gérer les décharge </title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

            <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="width: auto; padding-right: 30px; ">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

            <h3 class="sub-header" style="margin-top: 10px;"> Gérer les décharges</h3>

            <div>
                        <!-- Partie left -->
                        <div style="width: 400px; float:left; padding-right: 40px;">

                            <!-- Ajouter les décharges -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Ajouter les décharges</div>
                                <div class="panel-body">
                                    <form id="addDecharge" name="addDecharge" method="post" action="/admin_decharge/addDecharge">
                                    <table class="table">
                                        <tr>
                                            <td><label for="enseignant">Enseignants</label></td>
                                            <td>
                                            <select name="enseignant">
                                                <?php foreach($enseignants_add as $enseignant_add): ?>
                                                    <option value="<?php echo $enseignant_add['login']?>"><?php echo $enseignant_add['login']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for="decharge">Décharge </label></td>
                                            <td><input type="number" id="decharge" name="decharge" size="20" value="0" min="0" max="192"><br/></td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td><input type="submit" name="Submit" value="Ajouter"/></td>
                                        </tr>                           
                                    </table>
                                    </form>
                                </div>
                            </div>

                            <!-- Modifier les décharges -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Modifier les décharges</div>
                                <div class="panel-body">
                                    <form id="editDecharge" name="editDecharge" method="post" action="/admin_decharge/editDecharge">
                                    
                                    <!-- Si en état d'édition -->
                                    <?php if(isset($editDecharge['state']) && $editDecharge['state'] == 1): ?>  
                                            <table class="table">
                                                <tr>
                                                    <td><label for="enseignant">Enseignant</label></td>
                                                    <td>
                                                        <select name="enseignant">
                                                            <option value="<?php echo $editDecharge['data']['enseignant']; ?>">
                                                                <?php echo $editDecharge['data']['enseignant']; ?>      
                                                            </option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td><label for="decharge">Décharge</label></td>
                                                    <td><input type="number" id="decharge" name="decharge" size="20"
                                                        value="<?php echo $editDecharge['data']['decharge']; ?>"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <input type="submit" name="Submit" value="valider"/>
                                                        <a href="/admin_decharge">
                                                            <input type="button" name="Submit" value="Annuler"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>                
                                            
                                    <!-- Selectionner la decharge à éditer -->
                                    <?php else: ?>
                                    <table class="table">
                                        <tr>
                                            <td><label for="enseignant">Enseignant</label></td>
                                            <td>
                                                <select name="enseignant">
                                                    <?php foreach($decharges_info as $decharge_info): ?>
                                                    <option value="<?php echo $decharge_info['enseignant']?>"><?php echo $decharge_info['enseignant']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="hidden" name="editDecharge" value="1"/>
                                                <input type="submit" name="Submit" value="Modifier"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php endif; ?>
                                    </form>
                                </div>
                            </div>

                                <!-- Supprimer une décharge -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Supprimer une décharge</div>
                                <div class="panel-body">
                                    <form id="deleteDecharge" name="deleteDecharge" method="post" action="/admin_decharge/deleteDecharge">
                                    <table class="table">
                                        <tr>
                                            <td><label for="enseignant">Enseignants</label></td>
                                            <td>
                                                <select name="enseignant">
                                                    <?php foreach($decharges_info as $decharge_del): ?>
                                                        <option value="<?php echo $decharge_del['enseignant']?>"><?php echo $decharge_del['enseignant']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td><input type="submit" name="Submit" value="Supprimer"/></td>
                                        </tr>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Partie right -->
                        <div id="table_right" style="float: left ">
                                <!-- Recherche des décharges -->
                            <div class="panel panel-default" >
                                <div class="panel-heading">Recherche des décharges</div>
                                <div class="panel-body">

                            <form id="searchDecharge" name="searchDecharge" method="post" action="/admin_decharge/searchDecharge">
                            <table class="table">
                                <tr>
                                    <td><label for="enseignant">Enseignants</label></td>
                                    <td>
                                        <select name="enseignant">
                                        <option value="">Tous</option>
                                        <?php foreach($enseignants_login as $enseignant): ?>
                                        <option value="<?php echo $enseignant['login']?>"><?php echo $enseignant['login']?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>                        
                                    <td></td>
                                    <td><input type="submit" name="Submit" value="Chercher"/></td>
                                </tr>
                            </table>
                            </form>
                            </div>
                            </div>

                            <!-- Les données -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Les données</div>
                                <div class="panel-body">

                                    <table class="table">
                                        <tr>
                                            <th>Enseignant</th>
                                            <th>Décharge</th>
                                        </tr>

                                      <?php foreach($decharges as $decharge): ?>
                                        <tr>
                                            <td><?php echo $decharge['enseignant']?></td>
                                            <td><?php echo $decharge['decharge'] ?></td>                       
                                        </tr>
                                     <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>

                        </div>  <!-- Fine partie right -->
                    </div> <!-- Fine partie left et right -->
                </div>
            </div>
        </div>

    <script type="text/javascript"
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>