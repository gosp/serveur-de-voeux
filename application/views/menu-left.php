<!-- menu de gauche -->
<!-- <div id="menu-left" class="col-sm-3 col-md-2 sidebar" style="display:none"> -->
<div id="menu-left" class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">

        <!-- Lien home -->
        <?php if($current_page == 'home'): ?>
        <li class="active" style="position:relative;">
            <a href="/home"><span class="glyphicon glyphicon-home"></span> Accueil</a>
            <div class="coin"></div>
        </li>
        <?php else: ?>
        <li style="position:relative;">
            <a href="/home"><span class="glyphicon glyphicon-home"></span> Accueil</a>
        </li>
        <?php endif ?>

        <!-- Lien home -->
        <?php if($current_page == 'main' && $connected): ?>
        <li class="active" style="position:relative;">
            <a href="/main"><span class="glyphicon glyphicon-th"></span> Liste des modules</a>
            <div class="coin"></div>
        </li>
        <?php elseif($connected): ?>
        <li style="position:relative;">
            <a href="/main"><span class="glyphicon glyphicon-th"></span> Liste des modules</a>
        </li>
        <?php endif ?>
        
        <!-- Lien Récapitulatif -->
        <?php if($current_page == 'summary' && $connected ): ?>
        <li class="active" style="position:relative;">
            <a href="/summary"><span class="glyphicon glyphicon-briefcase"></span> Récapitulatif</a>
            <div class="coin"></div>
        </li>
        <?php elseif($connected) : ?>
        <li style="position:relative;">
            <a href="/summary"><span class="glyphicon glyphicon-briefcase"></span> Récapitulatif</a>
        </li>
        <?php endif ?>

        <!-- Lien FAQ -->
        <?php if($current_page == 'questionsFAQ'): ?>
        <li class="active" style="position:relative;">
            <a href="/questionsFAQ"><span class="glyphicon glyphicon-question-sign"></span> FAQ</a>
            <div class="coin"></div>
        </li>
        <?php else : ?>
        <li style="position:relative;">
            <a href="/questionsFAQ"><span class="glyphicon glyphicon-question-sign"></span> FAQ</a>
        </li>
        <?php endif ?>

    </ul>
    <ul class="nav nav-sidebar">

        <!-- Lien Récapitulatif -->
    
        <?php 
        if($connected && $user['admin'] && (
            ($current_page == 'administration') 
            || ($current_page == 'admin_prof') 
            || ($current_page == 'admin_decharge') 
            || ($current_page == 'admin_statistique')
            || ($current_page == 'admin_modules')
            || ($current_page == 'admin_affectation') 
            || ($current_page == 'admin_export'))): 
        ?>
        <li class="active" style="position:relative;">
            <a href="/administration"><span class="glyphicon glyphicon-wrench"></span> Administration</a>
            <div class="coin"></div>
                    <div id="nav_menus">
            <ul class="administration">
                <li><a href="/administration">Utilisateurs</a></li>
                <li><a href="/admin_modules">Modules</a></li>
                <li><a href="/admin_decharge">Decharges</a></li>
                <li><a href="/admin_affectation">Affectations</a></li>
                <li><a href="/admin_export">Exports</a></li>
            </ul>
        </div>
        </li>
        <?php elseif($connected && $user['admin']): ?>
        <li style="position:relative;">
            <a href="/administration"><span class="glyphicon glyphicon-wrench"></span> Administration</a>
        </li>
        <?php endif ?>
    </ul>
</div>