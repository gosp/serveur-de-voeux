<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Serveur de voeux - Liste des modules</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

        <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

            <h3 class="sub-header" style="margin-top: 10px;">Liste des modules</h3>

            <span style="display:block; float:left; padding: 5px 10px;">Trier par : </span>
            <ul class="nav nav-pills">
                <?php if ($sort == "libelle"): ?>
                    <li class="active">
                        <a style="padding: 5px 10px;" href="/main/index/libelle">Alphabétique</a>
                    </li>
                <?php else: ?>
                    <li class="">
                        <a style="padding: 5px 10px;" href="/main/index/libelle">Alphabétique</a>
                    </li>
                <?php endif ?>
                <?php if ($sort == "semestre"): ?>
                    <li class="active">
                        <a style="padding: 5px 10px;" href="/main/index/semestre">Semestre</a>
                    </li>
                <?php else: ?>
                    <li class="">
                        <a style="padding: 5px 10px;" href="/main/index/semestre">Semestre</a>
                    </li>
                <?php endif ?>
                <?php if ($sort == "public"): ?>
                    <li class="active">
                        <a style="padding: 5px 10px;" href="/main/index/public">Public</a>
                    </li>
                <?php else: ?>
                    <li class="">
                        <a style="padding: 5px 10px;" href="/main/index/public">Public</a>
                    </li>
                <?php endif ?>
            </ul>

            <div class="row">
                <?php foreach($modules as $module): ?>
                <a href="/module_content/index/<?php echo $module['ident'];?>">
                    <div class="col-xs-6 col-md-3 card">
                      <span class="thumbnail" style="border:0; background-color: transparent">
                        <?php echo $module['libelle']."<br/><br/>"; ?>
                        <?php echo $module['public']." - "; ?>
                        <?php echo $module['semestre']."<br/><br/>"; ?>
                        <?php echo $module['nom']." ".$module['prenom']; ?>
                      </span>
                      <div class="progress">
                        <?php if ($module['pourcentage'] <= 0): ?>
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">0%</div>
                            </div>
                        <?php elseif($module['pourcentage'] >= 100) : ?>
                            <div 
                                class="progress-bar progress-bar-success" 
                                role="progressbar" 
                                aria-valuenow="<?php echo $module['pourcentage']; ?>" 
                                aria-valuemin="0" aria-valuemax="100" 
                                style="width: <?php echo $module['pourcentage']; ?>%;">
                                <?php echo $module['pourcentage']; ?>%</div>
                            </div>                        
                        <?php else : ?>
                            <div 
                                class="progress-bar" 
                                role="progressbar" 
                                aria-valuenow="<?php echo $module['pourcentage']; ?>" 
                                aria-valuemin="0" aria-valuemax="100" 
                                style="width: <?php echo $module['pourcentage']; ?>%;">
                                <?php echo $module['pourcentage']; ?>%</div>
                            </div>
                        <?php endif ?>
                    </div>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
        <hr/>
      </div>
    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>