<!-- FlashMessages (après redirection) -->

	<!-- Message de succès -->
	<?php if($this->session->flashdata('success_message')): ?>
	<div class="alert alert-success alert-dismissable" style="border-color: #C6E6E9;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-ok-sign"></span></strong> 
	    <?php echo $this->session->flashdata('success_message'); ?>
	</div>

	<!-- Message d'erreur -->
	<?php elseif($this->session->flashdata('error_message')): ?>
	<div class="alert alert-danger alert-dismissable" style="border-color: #ECB9C2;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-exclamation-sign"></span></strong> 
	    <?php echo $this->session->flashdata('error_message'); ?>
	</div>

	<!-- Message d'information -->
	<?php elseif($this->session->flashdata('info_message')): ?>
	<div class="alert alert-info alert-dismissable" style="border-color: #BCD8F1;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-info-sign"></span></strong> 
	    <?php echo $this->session->flashdata('info_message'); ?>
	</div>
	<?php endif; ?>

<!-- PageMessages -->

	<!-- Message de succès -->
	<?php if(isset($this->data['_success_message'])): ?>
	<div class="alert alert-success alert-dismissable" style="border-color: #C6E6E9;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-ok-sign"></span></strong> 
	    <?php echo $this->data['_success_message']; ?>
	</div>

	<!-- Message d'erreur -->
	<?php elseif(isset($this->data['_error_message'])): ?>
	<div class="alert alert-danger alert-dismissable" style="border-color: #ECB9C2;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-exclamation-sign"></span></strong> 
	    <?php echo $this->data['_error_message']; ?>
	</div>

	<!-- Message d'information -->
	<?php elseif(isset($this->data['_info_message'])): ?>
	<div class="alert alert-info alert-dismissable" style="border-color: #BCD8F1;">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="false">&times;</button>
	    <strong><span class="glyphicon glyphicon-info-sign"></span></strong> 
	    <?php echo $this->data['_info_message']; ?>
	</div>
	<?php endif; ?>

