
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link href="/assets/css/dashboard.css" rel="stylesheet">
	<link href="/assets/css/main.css" rel="stylesheet">
	<title>Administration - Export de données au format CSV </title>
</head>

<body>
	<!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

            <!-- Menu de gauche -->
            <?php $this->load->view('menu-left.php'); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- Messages d'alerte -->
            <?php $this->load->view('alert.php'); ?>
                <h3 class="sub-header" style="margin-top: 10px;">Exporter les données au format CSV</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<h4>Veuillez choisir les données à exporter au format CSV :  </h4>
                            <br>
							<form method="post" action="/admin_export">
							    <p>
							        <input type="checkbox" <?php if(isset($modules)) echo "checked='checked'"; ?> name="module" id="mod" /> 
							        <label for="mod">Liste des modules</label><br />
                                    <?php if (isset($modules)): ?>
                                        <textarea rows="10" cols="100"><?php echo $modules ?></textarea><br /><br /><hr>
                                    <?php endif ?>
                                    
							        <input type="checkbox" <?php if(isset($enseignants)) echo "checked='checked'"; ?> name="enseignant" id="teach" /> 
							        <label for="teach">Liste des enseignants</label><br />
                                    <?php if (isset($enseignants)): ?>
                                        <textarea rows="10" cols="100"><?php echo $enseignants ?></textarea><br /><br /><hr>
                                    <?php endif ?>

							        <input type="checkbox" <?php if(isset($affectations)) echo "checked='checked'"; ?> name="affectation" id="affec" /> 
							        <label for="affec">Liste des affectations</label><br />
                                    <?php if (isset($affectations)): ?>
                                        <textarea rows="10" cols="100"><?php echo $affectations ?></textarea><br /><br /><hr>
                                    <?php endif ?>

							        <input type="checkbox" <?php if(isset($decharges)) echo "checked='checked'"; ?> name="decharge" id="decharg" /> 
							        <label for="decharg">Décharges</label><br/ >
                                    <?php if (isset($decharges)): ?>
                                        <textarea rows="10" cols="100"><?php echo $decharges ?></textarea><br /><br /><hr>
                                    <?php endif ?>                                    
							    </p>
                                <!-- Bouton visualiser -->
                                <button class="btn btn btn-primary" type="submit" name="preview">
                                    Visualiser
                                </button>

                                <!-- Bouton télécharger -->
                                <button class="btn btn btn-success" type="submit" name="download">
                                    Télécharger
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            <hr/>
        </div>
    </div>

	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>