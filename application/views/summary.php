﻿<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Serveur de voeux</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

        <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

        <!-- Recherche d'un autre enseignant -->
            <div class="panel panel-default pull-right" id='rechercheModule'>
                <div class="panel-body" id='creer_table'><legend>Rechercher un autre enseignant</legend>
                    <form id="chercherModule" method="post" action="/summary/showUserRecap">
                        <p>
                            <select class="form-control" name="recap">
                                    <?php foreach($teachers_list as $teacher): ?>
                                        <option value="<?php echo $teacher['login']?>">
                                            <?php echo $teacher['prenom']." ".$teacher['nom']; ?>
                                        </option>
                                    <?php endforeach; ?>
                            </select><br/>
                            <button type="submit" class="btn btn-default">Rechercher</button>
                        </p>
                    </form>
                </div>
            </div>
            <div style="clear:both"></div>

            <h3 class="sub-header" style="margin-top: 10px;">Informations générales</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                <table>
                    <tr>
                        <td rowspan="3">
                            <img src="/assets/images/user_avatar.png" alt="user avatar"/>
                        </td>
                        <td>Enseignant : <?php echo $teacher_info['nom']." ".$teacher_info['prenom']; ?></td>
                    </tr>
                    <tr>
                        <td>Status : <?php echo $teacher_info['statut']; ?></td>
                    </tr>                    
                    <tr>
                        <td>Statutaire : <?php echo $teacher_info['statutaire']; ?></td>
                    </tr>
                </table>
                <br>
                </div>
            </div>

            <!-- Liste des modules -->
            <h3 class="sub-header" style="margin-top: 10px;">Liste des modules</h3>
            <?php if(count($modules) == 0) : ?>
                <div class="panel panel-default">
                    <div class="panel-heading">Liste des modules</div>
                    <div class="panel-body">
                        <?php if($this->session->userdata('login') == $teacher_info["login"]):?>
                            <span>Vous n'êtes inscrit à aucun module.</span>
                        <?php else : ?>
                            <span>Cet enseignant n'est inscrit à aucun module.</span>
                        <?php endif;?>
                    </div>
                </div>
            <?php else:
                foreach ($modules as $module):?>
            <div class="panel panel-default">
                <div class="panel-heading"><strong><?php echo $module['infos']['libelle']; ?></strong></div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>nombre d'heures</th>
                                <th>Type</th>
                                <th>Publique</th>
                                <th style="width:20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($module['parts'] as $part) : ?>
                            <tr>
                                <td><?php echo $part['partie']; ?></td>
                                <td><?php echo $part['hed']; ?></td>
                                <td><?php echo $part['type']; ?></td>
                                <td><?php echo $part['public']; ?></td>
                                <td>
                                <?php if ($this->session->userdata('connected')
                                        && $this->session->userdata('login') == $teacher_info['login']): ?>    
                                    <a href="<?php echo $part['links']["unsubscribeLink"]; ?>">
                                    <button class="btn btn btn-danger" type="submit">Se désinscrire</button>
                                    </a>
                                <?php else: ?>
                                    <span class='text-danger'>Aucune action possible</span>
                                <?php endif ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endforeach;?>
            <?php endif?>

            <h3 class="sub-header" style="margin-top: 10px;">Statistiques</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- Statistiques public -->
                    <table class="table">
                        <tr>
                            <td style="width:40px; border:0">
                            <?php echo "<img src='data:image/png;base64," . base64_encode( $stats['images']['public'] ) . "' />"; ?>
                            </td>
                            <td style="vertical-align:middle; border:0">
                                <?php foreach ($stats['legends']['public'] as $legend): ?>
                                    <div>
                                        <?php echo "<img src='data:image/png;base64," . base64_encode( $legend['img'] ) . "' />"; ?>
                                        <?php echo $legend['label']; ?></div>
                                <?php endforeach ?>
                            </td>
                            <td style="vertical-align:middle; border:0">                    
                                <table class="table" style="width:auto;">
                                    <thead>
                                      <tr>
                                        <th>Nombre modules enseignés</th>
                                        <th>Nombre de modules responsable</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td><?php echo $stats['modules']['module_affected_count']; ?></td>                
                                        <td><?php echo $stats['modules']['module_owner_count']; ?></td>
                                      </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div style="clear:both"></div>

                    <!-- Statistique type de cours -->
                    <table class="table">
                        <tr>
                            <td style="width:40px">
                            <?php echo "<img src='data:image/png;base64," . base64_encode( $stats['images']['type'] ) . "' />"; ?>
                            </td>
                            <td style="vertical-align:middle">
                                <?php foreach ($stats['legends']['type'] as $legend): ?>
                                    <div>
                                        <?php echo "<img src='data:image/png;base64," . base64_encode( $legend['img'] ) . "' />"; ?>
                                        <?php echo $legend['label']; ?></div>
                                <?php endforeach ?>
                            </td>
                            <td style="vertical-align:middle">
                                <!-- Statistiques parties (cours) -->
                                <table class="table" style="width:auto">
                                    <thead>
                                      <tr>
                                        <th>Nombre cours enseignés</th>
                                        <th>Nombre d'heures de cours</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td><?php echo $stats['parts']['part_affected_count']; ?></td>                
                                        <td><?php echo $stats['parts']['part_hour_count']; ?></td>
                                      </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div style="clear:both"></div>

                </div>
            </div>
        </div>
        <hr/>
      </div>
    </div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>