﻿<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Serveur de voeux - Détails module</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

        <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

        <!--Affichage du module de recherche -->
            <div class="panel panel-default pull-right" id='rechercheModule'>
                <div class="panel-body" id='creer_table'><legend>Rechercher un autre module</legend>
                    <form id="chercherModule" name="chercherModule" method="post" action="/module_content/searchModule">
                        <p>
                            <select class="form-control" name="nomModule">
                                <?php foreach($listeModules as $module): ?>
                                    <option value="<?php echo $module['ident']?>"><?php echo $module['libelle']?></option>
                                <?php endforeach; ?>
                            </select><br/>
                            <button type="submit" class="btn btn-default">Rechercher</button>
                        </p>
                    </form>
                </div>
            </div>
            <div style="clear:both"></div>

            <!--Affichage des informations relatives au module -->
            <h3 class="sub-header" style="margin-top: 10px;">Informations du module</h3>
            <div class="panel panel-default">
                <table class="table">
                    <tr>
                        <td>
                            Identifiant : <?php echo $module_information['ident'];?>
                        </td>
                        <td>
                            Public : <?php echo $module_information['public'];?>
                        </td>
                        <td>
                            Semestre : <?php echo $module_information['semestre'];?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Libellé : <?php echo $module_information['libelle'];?>
                        </td>
                        <td>
                            Responsable : 
                            <?php 
                                if(is_null($module_information['responsable'])) echo "Aucun";
                                else echo $module_information['prenom']." ".$module_information['nom'];
                            ?> 
                        </td>
                        <td>
                            Nombre d'heures total : <?php echo $module_hours;?>
                        </td>
                    </tr>
                </table>
                <br>
            </div>

            <!-- Liste des parties du module -->
            <h3 class="sub-header" style="margin-top: 10px;">Liste des parties du module</h3>

            <div class="panel panel-default">
                <div class="panel-heading">Contenu module</div>
                <div class="panel-body">
                    <?php if(count($module_content) == 0) : ?>
                        <span>Ce module ne contient aucune partie</span>
                    <?php else : ?>
                    <table class="table">
                        <thead>
                          <tr>
                            <th>Nom</th>
                            <th>Type</th>
                            <th>Nombre d'heures</th>
                            <th>Enseignant</th>
                            <th>Inscription</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach($module_content as $key=>$value): ?>

                            <?php if(is_null($value["enseignant"])): ?>
                                <tr class="unsubscribed" style="height:51px">
                                    <td><?php echo $value["partie"]?></td>
                                    <td><?php echo $value["type"]?></td>
                                    <td><?php echo $value["hed"]?></td>
                                    <td><?php echo $value["owner"];?></td>
                            <?php elseif(isset($this->data['user_info']['login']) && ($this->data['user_info']['login'] == $value["enseignant"])) : ?>
                                <tr class="subscribed" style="height:51px">
                                    <td><?php echo $value["partie"]?></td>
                                    <td><?php echo $value["type"]?></td>
                                    <td><?php echo $value["hed"]?></td>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <?php echo $value["owner"];?>
                                    </td>                                  
                            <?php else: ?>
                                <tr style="height:51px">
                                    <td><?php echo $value["partie"]?></td>
                                    <td><?php echo $value["type"]?></td>
                                    <td><?php echo $value["hed"]?></td>
                                    <td><?php echo $value["owner"];?></td>                                    
                            <?php endif; ?>
                            <td>
                            <?php if(is_null($value["enseignant"])):
                                if($this->session->userdata('connected')):
                                    if($this->session->userdata('actif')): ?>
                                        <a href="<?php echo $value["subscribeLink"]?>">
                                        <button class="btn btn btn-primary" type="submit">
                                            S'inscrire
                                        </button></a>
                                    <?php else : 
                                        echo "<span class='text-danger'>Vous devez être actif pour réserver un module</span>";
                                    endif;
                                else :
                                    echo "<span class='text-danger'>Vous devez être connecté(e) pour réserver un module</span>";
                                endif;
                            else :
                                if(isset($this->data['user_info']['login']) && ($this->data['user_info']['login'] == $value["enseignant"])): ?>
                                    <a href="<?php echo $value["unsubscribeLink"]?>">
                                        <button class="btn btn btn-danger" type="submit">
                                            Se désinscrire
                                        </button></a>
                            <?php else: ?>
                                <span class="glyphicon glyphicon-ok"></span>
                            <?php endif; ?>
                            <?php endif; ?>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>                  
                    <?php endif; ?>
                </div>
            </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js">
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>