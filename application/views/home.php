<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Serveur de voeux - Accueil</title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

            <!-- Menu de gauche -->
            <?php $this->load->view('menu-left.php'); ?>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- Messages d'alerte -->
            <?php $this->load->view('alert.php'); ?>
                <h3 class="sub-header" style="margin-top: 10px;">Accueil</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4 class="sub-header" style="margin-top: 10px;">Présentation</h4>
                            <p class="clearfix">     
                            Bienvenue sur le <strong>serveur de voeux</strong> de l'<a href="http://www.enssat.fr/">ENSSAT</a>.
                            Il s'agit d'un outil ayant pour objectif de simplifier la répartition des divers modules entre les différents professeurs. Ainsi, plusieurs fonctionnalités sont mises en place afin de vous faciliter la tâche.
                            </p>      
                            <hr />
                            <div class="row">
                                <div class="col-lg-4 text-center">
                                    <img class="img-circle" src="<?php echo base_url(); ?>/assets/images/accueil_recap.jpg" alt="Generic placeholder image">
                                    <h2>Récapitulatif</h2>
                                    <p class="text-justify">Vous avez accès à l'ensemble de vos <strong>informations personnelles</strong> ainsi que la liste des modules auxquels vous êtes inscrit, dans la partie <em>Récapitulatif</em>. Cette page permet également de visualiser d'autres enseignants grâce à une fonctionnalité de recherche. Sur votre propre profil, vous pourrez vous <strong>désinscrire</strong> des différents modules. Enfin, des <strong>statistiques</strong> par enseignant sont également accessibles. Cela fournit des informations au sujet de la répartition des publics et des types de cours (TD, TP, CM) en fonction des modules enseignés par le professeur. Vous y trouverez également le nombre de modules, de parties de module et d'heures de cours enseignés, ainsi que le nombre de modules dont l'enseignant est responsable.</p>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <img class="img-circle" src="<?php echo base_url(); ?>/assets/images/accueil_module.jpg" alt="Generic placeholder image">
                                    <h2>Liste des modules</h2>
                                    <p class="text-justify">Les fonctionnalités du <strong>serveur de voeux</strong> ne s'arrêtent pas là, vous avez également accès aux différents <strong>modules</strong> existants parmi ceux proposés par l'ENSSAT. Afin de les visualiser, il suffit de cliquer sur le lien <em>Liste des modules</em> du menu de gauche. Il est possible de <strong>trier les modules</strong> par ordre alphabétique, par semestre ou par public via l'usage de la fonctionnalité présente en haut de cette page. Lorsque vous cliquez sur l'un des modules, vous accédez à une troisième fonctionnalité.</p>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <img class="img-circle" src="<?php echo base_url(); ?>/assets/images/accueil_contenu.jpg" alt="Generic placeholder image">
                                    <h2>Contenu d'un module</h2>
                                    <p class="text-justify"> En effet, il est également possible de consulter les détails d'un module. Cette dernière page montre à l'utilisateur les <strong>informations caractéristiques d'un module</strong>, c'est-à-dire son identifiant, son libellé, son public, son responsable, son semestre ainsi que le nombre d'heures total que représente le module dans son intégralité. De plus, la liste des parties qui composent le module consulté est également présente sur cette page, avec la possibilité pour l'utilisateur connecté de <strong>s'inscrire</strong> aux différentes parties du module, si elles n'ont pas déjà été réservées au préalable. Bien entendu, l'utilisateur peut également se <strong>désinscrire</strong> des parties de module dont il est l'enseignant.</p>
                                </div>
                            </div>
                            <?php if(!$this->session->userdata('connected')): ?>
                                <h4 class="sub-header" style="margin-top: 10px;">Connectez vous</h4>
                                <p>
                                Bien entendu, afin de pouvoir profiter de ces fonctionnalités, il est nécessaire de vous <strong>authentifier</strong> en cliquant sur le lien disponible en haut à droite de la page ou sur <a href="/login">ce lien</a>.
                                </p>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <hr/>
        </div>
    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>