
<html lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link href="/assets/css/dashboard.css" rel="stylesheet">
<link href="/assets/css/main.css" rel="stylesheet">
<title>Administration - Gérer l'affectation</title>
</head>

<?php $types = array("TD", "CM", "TP"); ?>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

        <div class="row">

            <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="width: auto; padding-right: 30px; ">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

            <h3 class="sub-header" style="margin-top: 10px;"> Gérer l'affectation</h3>

            <div>
                        <!-- Partie left -->
                        <div style="width: 400px; float:left; padding-right: 40px;">

                            <!-- Ajouter une affectation -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Ajouter une partie</div>
                                <div class="panel-body">
                                    <form id="addCourse" name="addCourse" method="post" action="/admin_affectation/addCourse">
                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module">
                                                <?php foreach($modules as $module): ?>
                                                <option value="<?php echo $module['ident']?>"><?php echo $module['ident']?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for="partie">Partie </label></td>
                                            <td><input type="text" id="partie" name="partie" size="20"/><br/></td>
                                        </tr>

                                        <tr>
                                            <td><label for="type">Type </label></td>
                                            <td><input type="text" id="type" name="type" size="20"/><br/></td>
                                        </tr> 

                                        <tr>
                                            <td><label for="hed">Heures </label></td>
                                            <td><input type="number" id="hed" min="0" max="999" name="hed" size="20" value="12"/><br/></td>
                                        </tr>

                                        <tr>
                                            <td><label for="enseignant">Enseignant </label></td>
                                            <td>
                                                <select name="enseignant">
                                                <option value="null">Aucun</option>
                                                <?php foreach($enseignants as $enseignant): ?>
                                                <option value="<?php echo $enseignant['login']?>"><?php echo $enseignant['login']?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr> 

                                        <tr>
                                            <td></td>
                                            <td><input type="submit" name="Submit" value="Ajouter"/></td>
                                        </tr>                           
                                    </table>
                                    </form>
                                </div>
                            </div>


                                <!-- Modifier une affectation -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Modifier une affectation</div>
                                <div class="panel-body">
                                    <form id="editCourse" name="editCourse" method="post" action="/admin_affectation/editCourse">

                                    <!-- Si en état d'édition -->
                                    <?php if(isset($edit1Course['state']) && $edit1Course['state'] == 1): ?>
                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module">
                                                    <option value="<?php echo $edit1Course['data']['selected_module']; ?>">
                                                        <?php echo $edit1Course['data']['selected_module']; ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for="partie">Partie</label></td>
                                            <td>
                                                <select name="partie">
                                                <?php foreach($edit1Course['data']['content'] as $part): ?>
                                                    <option value="<?php echo $part['partie']; ?>"><?php echo $part['partie']; ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="hidden" name="edit2Course" value="1"/>
                                                <input type="submit" name="Submit" value="Modifier"/>
                                                <a href="/admin_affectation/index">
                                                    <input type="button" name="Submit" value="Annuler"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

                                    <?php elseif(isset($edit2Course['state']) && $edit2Course['state'] == 1) : ?>

                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module">
                                                    <option value="<?php echo $edit2Course['data']['selected_module']; ?>">
                                                        <?php echo $edit2Course['data']['selected_module']; ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for="partie">Partie</label></td>
                                            <td>                                                
                                                <select name="partie">
                                                    <option value="<?php echo $edit2Course['data']['selected_partie']; ?>">
                                                        <?php echo $edit2Course['data']['selected_partie']; ?>
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label for="type">Type</label></td>
                                            <td>
                                                <select name="type" id="type">
                                                <?php foreach($types as $type): ?>
                                                <?php if($type == $edit2Course['data']['content']['type']) : ?>
                                                    <option selected="selected" value="<?php echo $edit2Course['data']['content']['type']; ?>"><?php echo $edit2Course['data']['content']['type']; ?></option>
                                                <?php else : ?>
                                                    <option  value="<?php echo $type; ?>"><?php echo $type; ?></option>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                                </select>
                                        </tr>

                                        <tr>
                                            <td><label for="hed">Heures</label></td>
                                            <td><input type="number" id="hed" name="hed" size="20"
                                                value="<?php echo $edit2Course['data']['content']['hed']; ?>"/>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td><label for="enseignant">Enseignant</label></td>
                                            <td>
                                                <select name="enseignant">
                                                    <option value="null">Aucun</option>
                                                    <?php foreach($enseignants as $enseignant): ?>
                                                        <option value="<?php echo $enseignant['login'];?>"><?php echo $enseignant['login'];?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="submit" name="Submit" value="valider"/>
                                                <a href="/admin_affectation/index">
                                                    <input type="button" name="Submit" value="Annuler"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

                                    <!-- Selectionner l'affectation à éditer -->
                                    <?php else: ?>
                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module">
                                                    <?php foreach($modules as $contenu): ?>
                                                    <option value="<?php echo $contenu['ident']?>"><?php echo $contenu['ident']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="hidden" name="edit1Course" value="1"/>
                                                <input type="submit" name="Submit" value="Modifier"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php endif; ?>
                                    </form>
                                </div>
                            </div>

                              
                            <!-- Supprimer une affectation -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Supprimer une affectation</div>
                                <div class="panel-body">
                                    <form id="deleteCourse" name="deleteCourse" method="post" action="/admin_affectation/deleteCourse">
                                    <?php if(isset($deleteCourse['state']) && $deleteCourse['state'] == 1):?>
                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module" id="module">
                                                        <option value="<?php echo $deleteCourse['data']['selected_module'];?>"><?php echo $deleteCourse['data']['selected_module'];?>
                                                        </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="partie">Partie</label>
                                            <td>
                                            <?php if(count($deleteCourse['data']['content']) > 0) : ?>
                                            <select name="partie" id="partie">
                                                <?php foreach($deleteCourse['data']['content'] as $module): ?>
                                                    <option value="<?php echo $module['partie']?>"><?php echo $module['partie']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <?php else : ?>
                                            Aucune partie
                                            <?php endif; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td><input type="submit" name="Submit" value="Supprimer"/>
                                            <a href="/admin_affectation">
                                                    <input type="button" name="Submit" value="Annuler"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php else: ?>
                                    <table class="table">
                                        <tr>
                                            <td><label for="module">Module</label></td>
                                            <td>
                                                <select name="module">
                                                    <?php foreach($modules as $module): ?>
                                                        <option value="<?php echo $module['ident']?>"><?php echo $module['ident']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="hidden" name="deleteCourse" value="1"/>
                                                <input type="submit" name="Submit" value="Modifier"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php endif; ?>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Partie right -->
                        <div id="table_right" style="float: left ">
                                <!-- Recherche une partie -->
                            <div class="panel panel-default" >
                                <div class="panel-heading">Recherche des parties</div>
                                <div class="panel-body">

                            <form id="searchCourse" name="searchCourse" method="post" action="/admin_affectation/searchCourse">
                            <table class="table">
                                <tr>
                                    <td><label for="module">Module</label></td>
                                    <td>
                                        <select name="module">
                                            <option value="null">Tous</option>
                                            <?php foreach($modules as $module): ?>
                                            <option value="<?php echo $module['ident']; ?>"><?php echo $module['ident']; ?>
                                            </option>
                                                <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="partie">Partie</label>
                                    <td>
                                        <select name="partie">
                                            <option value="null">Tous</option>
                                            <?php foreach($parties as $partie): ?>
                                            <option value="<?php echo $partie['partie']?>"><?php echo $partie['partie']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="type">Type</label>
                                    <td>
                                        <select name="type" id="type">
                                            <option value="null">Tous</option>
                                            <?php foreach($types as $type): ?>
                                                <option value="<?php echo $type?>"><?php echo $type; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="enseignant">Enseignant</label></td>
                                    <td>
                                    <select name="enseignant">
                                        <option value="null">Tous</option>
                                        <?php foreach($enseignants as $enseignant): ?>
                                            <option value="<?php echo $enseignant['login']?>"><?php echo $enseignant['login']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    </td>
                                </tr>

                                <tr>                        
                                    <td></td>
                                    <td>
                                        <input type="hidden" name="lol" value="xd">
                                        <input type="submit" name="Submit" value="Chercher"/></td>
                                </tr>
                            </table>
                            </form>
                            </div>
                            </div>

                            <!-- Les données -->
                            <div class="panel panel-default">
                                <div class="panel-heading">Les données</div>
                                <div class="panel-body">

                                    <table class="table">
                                        <tr>
                                            <th>Module</th>
                                            <th>Partie</th>
                                            <th>Type</th>
                                            <th>Heures</th>
                                            <th>Enseignant</th>
                                        </tr>

                                        <?php foreach($course_info as $course): ?>
                                        <tr>
                                            <td><?php echo $course['module'] ?></td>
                                            <td><?php echo $course['partie'] ?></td>
                                            <td><?php echo $course['type'] ?></td>
                                            <td><?php echo $course['hed'] ?></td>
                                            <td><?php echo $course['enseignant'] ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                            
                        </div>  <!-- Fine partie right -->
                    </div> <!-- Fine partie left et right -->
                </div>
            </div>
        </div>

    <script type="text/javascript"
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>