
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <title>Administration - Gérer les modules </title>
</head>

<body>
    <!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <?php $semestre_lists = array("S1", "S2"); ?>

    <div class="container-fluid">

        <div class="row">

            <!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="width: auto; padding-right: 30px; ">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>
            <h3 class="sub-header" style="margin-top: 10px;"> Gérer les modules</h3>

            <div>

                <!-- Partie left -->
                <div style="width: 1000px; float:left; padding-right: 40px;">

                        <!-- Ajouter un module -->
                    <div class="panel panel-default" id="module_struct">
                        <div class="panel-heading">Ajouter un module</div>
                        <div class="panel-body">
                            <form id="addModule" name="addModule" method="post" action="/admin_modules/addModule">
                            <table class="table">
                                <tr>
                                    <td><label for="ident">Identifiant</label></td>
                                    <td><input type="text" id="ident" name="ident" size="20"/><br/></td>
                                </tr>

                                <tr>
                                    <td><label for="public">Public</label></td>
                                    <td><input type="text" id="public" name="public" size="20"/><br/></td>
                                </tr>

                                <tr>
                                    <td><label for="semestre">Semestre</label></td>
                                    <td>
                                        <select name="semestre">
                                                <?php foreach($semestre_lists as $semestre_list): ?>
                                                <option value="<?php echo $semestre_list?>"><?php echo $semestre_list?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="libelle">Libelle</label></td>
                                    <td><input type="text" id="libelle" name="libelle" size="35"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="responsable">Responsable</label></td>
                                    <td>
                                        <select name="responsable">
                                            <option value="null">Aucun</option>
                                            <?php foreach($enseignants as $enseignant): ?>
                                                <option value="<?php echo $enseignant['login']?>"><?php echo $enseignant['login']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><input type="submit" name="Submit" value="Ajouter"/></td>
                                </tr>                           
                            </table>
                            </form>
                        </div>
                    </div>

                    <!-- Modifier un module -->
                    <div class="panel panel-default" id='module_sprim'>
                        <div class="panel-heading">Modifier un module</div>
                        <div class="panel-body">
                            <form id="editModule" name="editModule" method="post" action="/admin_modules/editModule">

                            <!-- Si en état d'édition -->
                            <?php if(isset($editModule['state']) && $editModule['state'] == 1): ?>  
                            <table class="table">
                                <tr>
                                    <td><label for="ident">Identifiant</label></td>
                                    <td>
                                        <select name="ident">
                                            <option value="<?php echo $editModule['data']['ident']; ?>">
                                                <?php echo $editModule['data']['ident']; ?>
                                            </option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="public">Public</label></td>
                                    <td><input type="text" id="public" name="public" size="20"
                                        value="<?php echo $editModule['data']['public']; ?>"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="semestre">Semestre</label></td>
                                    <td>
                                        <select name="semestre">
                                                <?php foreach($semestre_lists as $semestre_list): ?>
                                                <?php if($editModule['data']['semestre'] == $semestre_list): ?>
                                                <option selected="selected" value="<?php echo $semestre_list?>"><?php echo $semestre_list; ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $semestre_list?>"><?php echo $semestre_list; ?></option>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="libelle">Libelle</label></td>
                                    <td><input type="text" id="libelle" name="libelle" size="35"
                                        value="<?php echo $editModule['data']['libelle']; ?>"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label for="responsable">Responsable</label></td>
                                    <td>
                                        <select name="responsable">
                                            <option value="<?php echo $editModule['data']['responsable']; ?>">
                                                <?php echo $editModule['data']['responsable']; ?>
                                            </option>
                                            <option value="null">Aucun</option>
                                            <?php foreach($enseignants as $enseignant): ?>
                                                <option value="<?php echo $enseignant['login']?>"><?php echo $enseignant['login']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="submit" name="Submit" value="valider"/>
                                        <a href="/admin_modules/index">
                                            <input type="button" name="Submit" value="Annuler"/>
                                        </a>
                                    </td>
                                </tr>
                            </table>

                            <!-- Selectionner le module à éditer -->
                            <?php else: ?>
                            <table class="table">
                                <tr>
                                    <td><label for="ident">Identifiant</label></td>
                                    <td>
                                        <select name="ident">
                                            <?php foreach($modules as $module): ?>
                                            <option value="<?php echo $module['ident']?>"><?php echo $module['ident']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="hidden" name="editModule" value="1"/>
                                        <input type="submit" name="Submit" value="Modifier"/>
                                    </td>
                                </tr>
                            </table>
                            <?php endif; ?>
                            </form>
                        </div>
                    </div>
                </div>
                
                <!-- Partie right -->
                <div id="table_right" style="float: left ">


                    <!-- Supprimer un module -->
                    <div class="panel panel-default" >
                        <div class="panel-heading">Supprimer un module</div>
                        <div class="panel-body">
                            <form id="deleteModule" name="deleteModule" method="post" action="/admin_modules/deleteModule">
                            <table class="table">
                                <tr>
                                    <td><label for="ident">Identifiant</label></td>
                                    <td>
                                    <select name="ident">
                                        <?php foreach($modules as $module): ?>
                                        <option value="<?php echo $module['ident']?>"><?php echo $module['ident']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><input type="submit" name="Submit" value="Supprimer"/></td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                    <!-- Recherche un module -->
                    <div class="panel panel-default" >
                        <div class="panel-heading">Recherche module</div>
                        <div class="panel-body">

                    <form id="searchModule" name="searchModule" method="post" action="/admin_modules/searchModule">
                    <table class="table">
                        <tr>
                            <td><label for="ident">Identifiant</label></td>
                            <td>
                                <select name="ident">
                                    <option value="">Tous</option>
                                        <?php foreach($modules as $module): ?>
                                    <option value="<?php echo $module['ident']?>"><?php echo $module['ident']?></option>
                                <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="public">Public</label></td>
                            <td>
                                <select name="public">
                                    <option value="">Tous</option>
                                        <?php foreach($public_list as $public): ?>
                                    <option value="<?php echo $public['public']?>"><?php echo $public['public']?></option>
                                <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="semestre">Semestre</label></td>
                            <td>
                                <select name="semestre">
                                    <option value="">Tous</option>
                                    <?php foreach($semestre_lists as $semestre_list): ?>
                                    <option value="<?php echo $semestre_list?>"><?php echo $semestre_list?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>

                        <tr>                        
                            <td></td>
                            <td><input type="submit" name="Submit" value="Chercher"/></td>
                        </tr>
                    </table>
                    </form>
                    </div>
                    </div>

                    <!-- Les données -->
                    <div class="panel panel-default">
                        <div class="panel-heading">Les données</div>
                        <div class="panel-body">

                            <table class="table">
                                <tr>
                                    <th>Module Ident</th>
                                    <th width="30" >Public</th>
                                    <th width="30">Semestre</th>
                                    <th width="140">Libelle</th>
                                    <th width="40">Responsable</th>
                                </tr>
                                    <?php foreach($modules_info as $module_info): ?>
                                    <tr>
                                        <td><?php echo $module_info['ident'] ?></td>
                                        <td><?php echo $module_info['public'] ?></td>
                                        <td><?php echo $module_info['semestre'] ?></td>
                                        <td><?php echo $module_info['libelle'] ?></td>
                                        <td><?php echo $module_info['responsable'] ?></td>                  
                                    </tr>
                                    <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>  <!-- Fine partie right -->
            </div> <!-- Fine partie left et right -->
        </div>
    </div>
</div>

    <script type="text/javascript"
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>