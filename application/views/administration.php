<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link href="/assets/css/dashboard.css" rel="stylesheet">
	<link href="/assets/css/main.css" rel="stylesheet">
	<title>Administration - Gérer les enseignants </title>
</head>

<body>
	<!-- Header de la page -->
    <?php $this->load->view('header.php'); ?>

    <div class="container-fluid">

		<div class="row">

			<!-- Menu de gauche -->
        <?php $this->load->view('menu-left.php'); ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="width: auto; padding-right: 30px; ">

        <!-- Messages d'alerte -->
        <?php $this->load->view('alert.php'); ?>

			<h3 class="sub-header" style="margin-top: 10px;"> Gérer les enseignants</h3>

			<div>
						<!-- Partie left -->
						<div style="width: 400px; float:left; padding-right: 40px;">
									<!-- Ajouter un enseignant -->
									<div class="panel panel-default">
										<div class="panel-heading">Ajouter un enseignant</div>
										<div class="panel-body">
											<form id="addProf" name="addProf" method="post" action="/administration/addProf">
											<table class="table">
												<tr>
													<td><label for="login">Login </label></td>
													<td><input type="text" id="login" name="login" size="20"/><br/></td>
												</tr>
												<tr>
													<td><label for="pwd">Mot de passe </label></td>
													<td><input type="password" id="pwd" name="pwd" size="16"/><br/></td>
												</tr>
												<tr>
													<td><label for="nom">Nom </label></td>
													<td><input type="text" id="nom" name="nom" size="20"/><br/></td>
												</tr>
												<tr>
													<td><label for="prenom">Prenom </label></td>
													<td><input type="text" id="prenom" name="prenom" size="20"/><br/></td>
												</tr>

												<tr>
													<td><label for="statut">Statut </label></td>
													<td><input type="text" id="statut" name="statut" size="20" value="titulaire"/><br/></td>
												</tr>

												<tr>
													<td><label for="statutaire">Statutaire </label></td>
													<td><input type="number" min="0" max="192" id="statutaire" name="statutaire" size="20" value="192"/><br/></td>
												</tr>

												<tr>
													<td><label for="actif">Actif </label></td>
													<td><input type="number" min="0" max="1" id="actif" name="actif" size="20" value="1"/><br/></td>
												</tr>

												<tr>
													<td><label for="administrateur">Administrateur </label></td>
													<td><input type="number" min="0" max="1" id="administrateur" name="administrateur" size="20" value="0"/><br/></td>
												</tr>

												<tr>
													<td></td>
													<td><input type="submit" name="Submit" value="Ajouter"/></td>
												</tr>							
											</table>
											</form>
										</div>
									</div>

									<!-- Modifier un enseignant -->
									<div class="panel panel-default">
										<div class="panel-heading">Modifier un enseignant</div>
										<div class="panel-body">
											<form id="editProf" name="editProf" method="post" action="/administration/editProf">

											<!-- Si en état d'édition -->
											<?php if(isset($editProf['state']) && $editProf['state'] == 1): ?>	
											<table class="table">
												<tr>
													<td><label for="login">Login</label></td>
													<td>
														<select name="login">
															<option value="<?php echo $editProf['data']['login']; ?>">
																<?php echo $editProf['data']['login']; ?>
															</option>
														</select>
													</td>
												</tr>
												<tr>
													<td><label for="pwd">Mot de passe</label></td>
													<td><input type="text" id="pwd" name="pwd" size="16"
														value="<?php echo $editProf['data']['pwd']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="nom">Nom</label>
													<td>
														<input type="text" id="nom" name="nom" size="20"
														value="<?php echo $editProf['data']['nom']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="prenom">Prenom</label>
													<td><input type="text" id="prenom" name="prenom" size="20"
														value="<?php echo $editProf['data']['prenom']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="statut">statut</label>
													<td><input type="text" id="statut" name="statut" size="20"
														value="<?php echo $editProf['data']['statut']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="statutaire">statutaire</label>
													<td><input type="number" id="statutaire" min="0" max="192" name="statutaire" size="20"
														value="<?php echo $editProf['data']['statutaire']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="actif">actif</label>
													<td><input type="number" id="actif" min="0" max="1" name="actif" size="20"
														value="<?php echo $editProf['data']['actif']; ?>"/>
													</td>
												</tr>

												<tr>
													<td><label for="administrateur">administrateur</label>
													<td><input type="number" id="administrateur" min="0" max="1" name="administrateur" size="20"
														value="<?php echo $editProf['data']['administrateur']; ?>"/>
													</td>
												</tr>

												<tr>
													<td></td>
													<td>
														<input type="submit" name="Submit" value="valider"/>
														<a href="/administration/index">
															<input type="button" name="Submit" value="Annuler"/>
														</a>
													</td>
												</tr>
											</table>

											<!-- Selectionner l'enseignant à éditer -->
											<?php else: ?>
											<table class="table">
												<tr>
													<td><label for="login">Login</label></td>
													<td>
														<select name="login">
														<?php foreach($enseignants as $enseignant): ?>
															<option value="<?php echo $enseignant['login']?>">
																<?php echo $enseignant['login']?>
															</option>
					                					<?php endforeach; ?>
														</select>
													</td>
												</tr>
												<tr>
													<td></td>
													<td>
														<input type="hidden" name="editProf" value="1"/>
														<input type="submit" name="Submit" value="Modifier"/>
													</td>
												</tr>
											</table>
											<?php endif; ?>
											</form>
										</div>
									</div>

									<!-- Supprimer un enseignant -->
									<div class="panel panel-default">
										<div class="panel-heading">Supprimer un enseignant</div>
										<div class="panel-body">
											<form id="deleteProf" name="deleteProf" method="post" action="/administration/deleteProf">
											<table class="table">
												<tr>
													<td><label for="login">Login</label></td>
													<td>
														<select name="login">
															<?php foreach($enseignants as $enseignant): ?>
															<option value="<?php echo $enseignant['login']?>">
																<?php echo $enseignant['login']?>
															</option>
					                						<?php endforeach; ?>
														</select>
													</td>
												</tr>

												<tr>
													<td></td>
													<td><input type="submit" name="Submit" value="Supprimer"/></td>
												</tr>
											</table>
											</form>
										</div>
									</div>
						</div>

						<!-- Partie right -->
						<div id="table_right" style="float: left ">
								<!-- Recherche un enseignant -->
							<div class="panel panel-default" >
								<div class="panel-heading">Rechercher un enseignant</div>
								<div class="panel-body">

							<form id="searchProf" name="searchProf" method="post" action="/administration/searchProf">
							<table class="table">
								<tr>
									<td><label for="login">Login</label></td>
									<td>
										<select name="login">
                                    		<option value="">Tous</option>
											<?php foreach($enseignants as $enseignant): ?>
											<option value="<?php echo $enseignant['login']?>">
												<?php echo $enseignant['login']?>
											</option>
								            <?php endforeach; ?>
										</select>
									</td>
								</tr>

								<tr>						
									<td><label for="nom">Nom</label></td>
									<td>							
										<select name="nom">
											<option value="">Tous</option>
											<?php foreach($enseignants as $enseignant): ?>
											<option value="<?php echo $enseignant['nom']?>">
												<?php echo $enseignant['nom']?>
											</option>
			                				<?php endforeach; ?>
										</select>
									</td>
								</tr>

								<tr>
								<td><label for="prenom">Prenom</label></td>
									<td>
										<select name="prenom">
											<option value="">Tous</option>
											<?php foreach($enseignants as $enseignant): ?>
											<option value="<?php echo $enseignant['prenom']?>">
												<?php echo $enseignant['prenom']?>
											</option>
				                			<?php endforeach; ?>
										</select>
									</td>
								</tr>

								<tr>						
									<td></td>
									<td><input type="submit" name="Submit" value="Chercher"/></td>
								</tr>
							</table>
							</form>
							</div>
							</div>


							<!-- Les données -->
							<div class="panel panel-default">
								<div class="panel-heading">Les données</div>
								<div class="panel-body">

									<table class="table">
										<tr>
											<th>Login</th>
											<th>Nom</th>
											<th>Prenom</th>
											<th>Statut</th>
											<th>Statutaire</th>
											<th>actif</th>
											<th>Admin</th>
										</tr>

										<?php foreach($prof_info as $enseignant): ?>
										<tr>
													<td><?php echo $enseignant['login'] ?></td>
													<td><?php echo $enseignant['nom'] ?></td>
													<td><?php echo $enseignant['prenom'] ?></td>
													<td><?php echo $enseignant['statut'] ?></td>
													<td><?php echo $enseignant['statutaire'] ?></td>
													<td><?php echo $enseignant['actif'] ?></td>
													<td><?php echo $enseignant['administrateur'] ?></td>						
												</tr>
				                		<?php endforeach; ?>
									</table>
								</div>
							</div>
						</div>	<!-- Fine partie right -->
					</div> <!-- Fine partie left et right -->
				</div>
			</div>
		</div>

	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>